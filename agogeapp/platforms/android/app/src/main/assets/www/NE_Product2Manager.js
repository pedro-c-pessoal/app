function NEProduct2Manager() 
{

    this.product2 = {};

    this.createProduct2 = () => 
    {
        this.product2 = 
        {
            name: ''
        };

        NEBuildScreen.product2(this.product2);
    }

    this.saveProduct2 = () => 
    {

        NEApplication.showLoading(true);

        this.product2.name = $('#product2-name').val();

        if (NEProduct2Manager.verifyFields()) 
        {

            var product2 = this.product2;

            let groupQuery = [];
            let product2BaseDMLInsert = 'INSERT INTO Product2 (PRODUCT_NAME) VALUES ';
            let product2BaseDMLUpdate = 'UPDATE Product2 SET ';
            let product2Query = '';

            if (product2.id) 
            {
                product2Query = `
                    ${product2BaseDMLUpdate}
                    PRODUCT2_NAME = '${product2.name}',
                    SYNC_STATUS = '0'
                    WHERE PRODUCT2_ID = '${product2.id}'
                `;
            } 
            else 
            {
                product2Query = `
                ${product2BaseDMLInsert}
                    (
                        '${SUB_stringEscape(product2.name)}', 
                        '${SUB_stringEscape(0)}'
                    )
                `;
            }

            product2Query = product2Query.replace(/(\r\n|\n|\r)/gm, "");

            groupQuery.push(product2Query);

            (new NEBulkSQL()).execute({
                statements: groupQuery,
                callback: function (resultSet, status) 
                {
                    if (status.fails == 0) 
                    {
                        swal("Produto salvo com sucesso!", { icon: "success" }).then(() => 
                        {
                            NEApplication.showLoading(false);
                            NEBuildScreen.product2Search();
                        });
                    } 
                    else 
                    {
                        swal("Erro na inserção do produto", { icon: "error" });
                        NEApplication.showLoading(false);
                    }
                }
            });

        } 
        else 
        {
            swal("Para salvar, preencha todos os campos obrigatórios: " + this.field, { icon: "warning" });
            NEApplication.showLoading(false);
        }

    }

    this.field = '';

    this.verifyFields = () => 
    {
        this.field = '';
        let requiredFields = 
        {
            'name': 'Nome'
        };
        let valid = true;
        Object.keys(requiredFields).forEach((field) => 
        {
            if (!this.product2[field]) {
                this.field = requiredFields[field];
                valid = false;
            }
        });
        return valid;
    }

    this.editProduct2 = (product2Id) => 
    {
        NEApplication.showLoading(true);
        NeDBManager.SUB_getProduct2ById(product2Id, (response) => 
        {
            let product2 = response[0];

            this.product2 = 
            {
                id: product2.PRODUCT2_ID,
                name: product2.PRODUCT2_NAME
            };

            NEApplication.showLoading(false);
            NEBuildScreen.product2(this.product2);
        });
    }

}

NEProduct2Manager.VAR_instance = null;
NEProduct2Manager.SUB_getInstance = function () 
{
    if (this.VAR_instance === null) this.VAR_instance = new NEProduct2Manager();
    return this.VAR_instance;
};
NEProduct2Manager.createProduct2 = function () 
{
    NEProduct2Manager.SUB_getInstance().createProduct2();
};
NEProduct2Manager.verifyFields = function () 
{
    return NEProduct2Manager.SUB_getInstance().verifyFields();
};
NEProduct2Manager.addPricebook = function (priceBook2Id, priceBook2Name)
{
    return NEProduct2Manager.SUB_getInstance().addPricebook(priceBook2Id, priceBook2Name);
}
NEProduct2Manager.getStatus = function () 
{
    return NEProduct2Manager.SUB_getInstance().getStatus();
};
NEProduct2Manager.getShipping = function ()
{
    return NEProduct2Manager.SUB_getInstance().getShipping();
}
NEProduct2Manager.getPaymentMethod = function ()
{
    return NEProduct2Manager.SUB_getInstance().getPaymentMethod();
}
NEProduct2Manager.saveProduct2 = function () 
{
    NEProduct2Manager.SUB_getInstance().saveProduct2();
};
NEProduct2Manager.editProduct2 = function (product2Id) 
{
    NEProduct2Manager.SUB_getInstance().editProduct2(product2Id);
};
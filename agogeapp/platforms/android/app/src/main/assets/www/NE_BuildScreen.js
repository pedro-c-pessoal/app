function NEBuildScreen() {

    this.login = () => {

        NEApplication.showLoading(true);

        let html = `
             <div class="container-fluid bg-light">
                <div class="row">
                    <div class="col">
                        <div class="box-wrap-inicial">
                            <div class="brand">
                                <img src="img/nescara-logo.png" class="mb-3 mt-3 icon-mobile" alt="Mobile">
                            </div>
                            <button type="button" class="btn btn-block btn-cta-main" onclick="NEApplication.SUB_getInstance().loginScreenButton()">
                                <span>Acessar</span>
                                <img src="img/ic-arrow-right.svg" alt="Acessar">
                            </button>
                            <span style="color: #129fdb;font-size: 22px;font-weight: 600;text-align: center;">Treinamentos</span>
                        </div>
                    </div>
                </div>
            </div>
		`;

        $('#main').html(html);

        NEApplication.showLoading(false);
    }

    this.welcome = () => {

        NEApplication.showLoading(true);

        let html = `
			<!-- HEADER -->
            <header class="header">
                <div class="container">

                    <div class="row">
                        <div class="col-3">
                            <a href="#" class="brand-header">
                                <img src="img/nescara-logo.png" alt="Vendas App" style="max-width: 95px;">
                            </a>
                        </div>
                        <div class="col-9">
                            <div class="wrap-header">
                                <nav class="nav-header">
                                    <a class="text-secondary" onclick="NEApplication.logout()">${APP.systemInfo.userName}</a>
                                </nav>
                            </div>
                        </div>
                    </div>

                </div>
            </header>
            <!-- /HEADER -->

            <div id="modal_container"></div>

            <!-- CORPO DE CONTEÚDO -->
            <div id="body_content">
                <div class="wrap bg-welcome">
                    <div class="container">

                    </div>
                </div>
            </div>
            <!-- /CORPO DE CONTEÚDO -->


            <div class="version_container">
                <div class="version_text">Versão ${AppInfo.version}</div>
            </div>

            <!-- FOOTER -->
            <footer class="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="nav-footer">
                                <ul>
                                    <li>
                                        <a onclick="NEBuildScreen.welcome()">
                                            <img src="img/ic-home.svg" alt="Home">
                                            <span>Home</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a onclick="NEBuildScreen.orderSearch()">
                                            <img src="img/ic-clipboard.svg" alt="Pedidos">
                                            <span>Pedidos</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a onclick="NEBuildScreen.pricebookSearch()">
                                            <img src="img/ic-dollar.svg" alt="Catálogo de Preços">
                                            <span>Catálogo</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a onclick="NEApplication.sync()">
                                            <img src="img/ic-sync-cinza.svg" alt="Sync">
                                            <span>Sincronizar</span>
                                        </a>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- /FOOTER -->
		`;

        $('#main').html(html);

        NEApplication.showLoading(false);
    }

    //Pesquisa order 
    this.orderSearch = () =>
    {
        NEApplication.showLoading(true);
        let html = `
            <div class="wrap commom" style="position: absolute; width: 100%; height: 100%;">
                <div class="container" style="height: 100%;">

                    <div class="row">
                        <div class="col-12">
                            <div class="unique-data">
                                <div class="content" style="padding: 0px 5px 15px 5px">
                                    <button class="btn btn-cta-primary shadow mb-2" onclick="NEOrderManager.createOrder()">
                                        <span>Criar Pedido</span>
                                        <img src="img/ic-plus-circle-white.svg" alt="Criar Pedido">
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="box-lista-search" id="order_list_container">

                    </div>
                </div>
            </div>
        `;

        $('#body_content').html(html);

        NEQueryController.searchOrders();
    }

    //Desenha orders
    this.writeOrderList = (orders) => {
        let html = ``;

        if (orders.length == 0)
            listHTML = '';

        orders.forEach((order) => {
            html += `
                <div class="row">
                    <div class="mx-auto col-md-12 col-sm-12 col-xs-12 mb-2 mb-md-4">
                        <div class="card card-lista-pedido border-secondary">

                            <div class="card-body pt-3" onclick="NEOrderManager.editOrder('${order.ORDER_ID}')">

                                <div class="row">
                                    <div class="col-12" style="display: flex;justify-content: space-between;">
                                        <div class="unique-data">
                                            <div class="content" style="color: #1f68b1">
                                                ${order.ORDER_NAME}
                                            </div>
                                            <div class="lb-commom">
                                                Número interno do vendedor: ${order.ORDER_SELLERINTERNALNUMBER ? order.ORDER_SELLERINTERNALNUMBER : 'Não informado'}
                                            </div>
                                            <div class="lb-commom">
                                                Data de criação: ${order.ORDER_EFFECTIVEDATE ? order.ORDER_EFFECTIVEDATE : 'Não informado'}
                                            </div>
                                            <div class="lb-commom">
                                                Data de entrega: ${order.ORDER_DELIVERDATE ? order.ORDER_DELIVERDATE : 'Não informado'}
                                            </div>
                                            <div class="lb-commom">
                                                Status: ${order.ORDER_STATUS ? order.ORDER_STATUS : 'Não informado'}
                                            </div>
                                            <div class="lb-commom">
                                                Frete: ${order.ORDER_SHIPPING ? order.ORDER_SHIPPING : 'Não informado'}
                                            </div>
                                            <div class="lb-commom">
                                                Descrição: ${order.ORDER_DESCRIPTION ? order.ORDER_DESCRIPTION : 'Não informado'}
                                            </div>
                                            <div class="lb-commom">
                                                Forma de pagamento: ${order.ORDER_PAYMENTMETHOD ? order.ORDER_PAYMENTMETHOD : 'Não informado'}
                                            </div>
                                        </div>
                                        ${order.SYNC_STATUS == 0 ? `
                                            <div>
                                                <img src="img/ic-substituir.svg" alt="Sync">
                                            </div>
                                        ` : ``}
                                    </div>

                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            `;
        });

        $('#order_list_container').html(html);
        NEApplication.showLoading(false);
    }

    //Order
    this.order = (order) =>
    {
        NEApplication.showLoading(true);

        let html = `
        <div class="wrap commom">
            <div class="container">

                <div id="select_modal_container"></div>

                <div class="box-form-pedido">

                    <div class="row mt-3">
                        <div class="col-12">
                            <div class="unique-data">
                                <div class="lb-commom">
                                    Nome <span class="required">*</span>
                                </div>
                                <div class="content">
                                    <input id="order-name" type="text" class="form-control" value="${order.name}">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mt-3">
                        <div class="col-12">
                            <div class="unique-data">
                                <div class="lb-commom">
                                    Número Interno de Vendedor <span class="required">*</span>
                                </div>
                                <div class="content">
                                    <input id="order-sellerInternalNumber" type="number" class="form-control" value="${order.internalSellerNumber}">
                                </div>
                            </div>
                        </div>
                    </div>

                    <input id="order-ownerId" type="hidden" value="${APP.systemInfo.userIdx}"> 

                    <div class="row mt-3">
                            <div class="col-12">
                                <div class="unique-data">
                                    <div class="lb-commom">
                                        Cliente <span class="required">*</span>
                                    </div>
                                    <div class="content" onclick="NEBuildScreen.openSelectAccountModal()">
                                        <input id="order-account" type="text" class="form-control" readonly value="${order.accountId}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row mt-3">
                        <div class="col-12">
                            <div class="unique-data">
                                <div class="lb-commom">
                                    Data de início: <span class="required">*</span>
                                </div>
                                <div class="content">
                                    <input id="order-effectiveDate" type="date" class="form-control disabled" readonly>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mt-3">
                        <div class="col-12">
                            <div class="unique-data">
                                <div class="lb-commom">
                                    Data de entrega: <span class="required">*</span>
                                </div>
                                <div class="content">
                                    <input id="order-deliverDate" type="date" class="form-control" value="${order.deliverDate}">
                                </div>
                            </div>
                        </div>
                    </div>

                    <script>
                        document.getElementById('order-effectiveDate').value = new Date().toISOString().substring(0, 10);
                    </script>

                    <div class="row mt-3">
                            <div class="col-12">
                                <div class="unique-data">
                                    <div class="lb-commom">
                                        Status do pedido <span class="required">*</span>
                                    </div>
                                    <select id="order-status" class="form-control">

                                    </select>
                                </div>
                            </div>
                    </div>

                   <div class="row mt-3">
                            <div class="col-12">
                                <div class="unique-data">
                                    <div class="lb-commom">
                                        Tipo de frete <span class="required">*</span>
                                    </div>
                                    <select id="order-shipping" class="form-control">

                                    </select>
                                </div>
                            </div>
                    </div>

                    <div class="row mt-3">
                        <div class="col-12">
                            <div class="unique-data">
                                <div class="lb-commom">
                                    Descrição: <span class="required">*</span>
                                </div>
                                <div class="content">
                                    <input id="order-description" type="text" class="form-control" value="${order.description}">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mt-3">
                            <div class="col-12">
                                <div class="unique-data">
                                    <div class="lb-commom">
                                        Método de pagamento <span class="required">*</span>
                                    </div>
                                    <select id="order-paymentMethod" class="form-control">

                                    </select>
                                </div>
                            </div>
                    </div>


                <div class="row mb-3 mt-4">
                    <div class="col-6 col-xs-6">
                        <button class="btn btn-outline-primary btn-nav-steps p-3 btn-block" onclick="NEBuildScreen.orderSearch()">
                            <img src="img/ic-arrow-left.svg" class="ic-voltar" alt="Voltar">
                            <span>Voltar</span>
                        </button>
                    </div>
                    <div class="col-6 col-xs-6 text-right">
                        <button class="btn btn-primary btn-nav-steps-blue p-3 btn-block" onclick="NEOrderManager.saveOrder(${order.id})">
                            <span>Continuar</span>
                            <img src="img/ic-arrow-right.svg" class="ic-continuar" alt="Continuar">
                        </button>
                    </div>
                </div>

            </div>
        </div>
        `

        $('#body_content').html(html);

        NEQueryController.searchOrderStatus();
        NEQueryController.searchOrderShipping();
        NEQueryController.searchOrderPaymentMethod();

        NEApplication.showLoading(false);

        $(window).scrollTop(0);

    }

    this.orderItemSearch = (orderId) =>
    {
        NEApplication.showLoading(true);
        let html = `
            <div class="wrap commom" style="position: absolute; width: 100%; height: 100%;">
                <div class="container" style="height: 100%;">

                    <div class="row">
                        <div class="col-12">
                            <div class="unique-data">
                                <div class="content" style="padding: 0px 5px 15px 5px">
                                    <button class="btn btn-cta-primary shadow mb-2" onclick="NEOrderItemManager.createOrder(${orderId})">
                                        <span>Criar Produto do pedido</span>
                                        <img src="img/ic-plus-circle-white.svg" alt="Criar Produto do pedido">
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="box-lista-search" id="orderItem_list_container">

                    </div>
                </div>
            </div>
        `;

        $('#body_content').html(html);

        NEQueryController.searchOrderItems(orderId);
    }

    //Escreve lista de OrderItem
    this.writeOrderItemList = (orderItems, orderId) =>
    {
        let html = ``;

        if (orderitems.length == 0)
            listHTML = '';

        orderItems.forEach((orderItem) => {
            html += `
                <div class="row">
                    <div class="mx-auto col-md-12 col-sm-12 col-xs-12 mb-2 mb-md-4">
                        <div class="card card-lista-pedido border-secondary">

                            <div class="card-body pt-3" onclick="NEOrderItemManager.editOrderItem('${orderItem.ORDERITEM_ID}')">

                                <div class="row">
                                    <div class="col-12" style="display: flex;justify-content: space-between;">
                                        <div class="unique-data">
                                            <div class="content" style="color: #1f68b1">
                                                ${orderItem.ORDERITEM_ID}
                                            </div>
                                            <div class="lb-commom">
                                                Quantidade: ${orderItem.ORDERITEM_QUANTITY ? orderItem.ORDERITEM_QUANTITY : 'Não informado'}
                                            </div>
                                            <div class="lb-commom">
                                                Produto: ${orderItem.PRODUCT2_IDX ? orderItem.PRODUCT2_IDX : 'Não informado'}
                                            </div>
                                            <div class="lb-commom">
                                                Preço de lista: ${orderItem.ORDERITEM_LISTPRICE ? orderItem.ORDERITEM_LISTPRICE : 'Não informado'}
                                            </div>
                                            <div class="lb-commom">
                                                Preço de unidade: ${orderItem.ORDERITEM_UNITPRICE ? orderItem.ORDERITEM_UNITPRICE : 'Não informado'}
                                            </div>
                                            <div class="lb-commom">
                                                Desconto: ${orderItem.ORDERITEM_DISCOUNT ? orderItem.ORDERITEM_DISCOUNT : 'Não informado'}
                                            </div>
                                        </div>
                                        ${orderItem.SYNC_STATUS == 0 ? `
                                            <div>
                                                <img src="img/ic-substituir.svg" alt="Sync">
                                            </div>
                                        ` : ``}
                                    </div>

                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            `;
        });

        $('#orderItem_list_container').html(html);
        NEApplication.showLoading(false);
    }

    //OrderItems
    this.orderItem = (orderItem) =>
    {
        NEApplication.showLoading(true);

        let html = `
        <div class="wrap commom">
            <div class="container">

                <div id="select_modal_container"></div>

                <div class="box-form-pedido">

                    <div class="row mt-3">
                        <div class="col-12">
                            <div class="unique-data">
                                <div class="lb-commom">
                                    Nome <span class="required">*</span>
                                </div>
                                <div class="content">
                                    <input id="orderItem-name" type="text" class="form-control" value="${orderItem.name}">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mt-3">
                        <div class="col-12">
                            <div class="unique-data">
                                <div class="lb-commom">
                                    Quantidade <span class="required">*</span>
                                </div>
                                <div class="content">
                                    <input id="orderItem-quantity" type="text" class="form-control" value="${orderItem.quantity}">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mt-3">
                        <div class="col-12">
                            <div class="unique-data">
                                <div class="lb-commom">
                                    Preço de catálogo <span class="required">*</span>
                                </div>
                                <div class="content">
                                    <input id="orderItem-listPrice" type="text" class="form-control" value="${orderItem.listPrice}">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mt-3">
                        <div class="col-12">
                            <div class="unique-data">
                                <div class="lb-commom">
                                    Preço de venda <span class="required">*</span>
                                </div>
                                <div class="content">
                                    <input id="orderItem-unitPrice" type="text" class="form-control" value="${orderItem.unitPrice}">
                                </div>
                            </div>
                        </div>
                    </div>

            </div>
        </div>
        `

        $('#body_content').html(html);

        NEApplication.showLoading(false);

        $(window).scrollTop(0);
    }

    //Pesquisa Pricebook
    this.pricebookSearch = () =>
    {
        NEApplication.showLoading(true);
        let html = `
            <div class="wrap commom" style="position: absolute; width: 100%; height: 100%;">
                <div class="container" style="height: 100%;">

                    <div class="row">
                        <div class="col-12">
                            <div class="unique-data">
                                <div class="content" style="padding: 0px 5px 15px 5px">
                                    
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="box-lista-search" id="product_list_container">

                    </div>
                </div>
            </div>
        `;

        $('#body_content').html(html);

        NEQueryController.searchProducts();
    }

    //Desenha lista de products
    this.writeProductList = (products) => {
        let html = ``;

        if (products.length == 0)
            listHTML = '';

            products.forEach((product2) => {
            html += `
                <div class="row">
                    <div class="mx-auto col-md-12 col-sm-12 col-xs-12 mb-2 mb-md-4">
                        <div class="card card-lista-pedido border-secondary">

                            <div class="card-body pt-3">

                                <div class="row">
                                    <div class="col-12" style="display: flex;justify-content: space-between;">
                                        <div class="unique-data">
                                            <div class="content" style="color: #1f68b1">
                                                ${product2.PRODUCT_NAME}
                                            </div>
                                            <div class="lb-commom">
                                                Código do produto: ${product2.PRODUCT2_IDX ? product2.PRODUCT2_IDX : 'Não informado'}
                                            </div>
                                            <div class="lb-commom">
                                                Data de criação: ${product2.PRODUCT2_LISTPRICE ? product2.PRODUCT2_LISTPRICE : 'Não informado'}
                                            </div>
                                        </div>
                                        ${product2.SYNC_STATUS == 0 ? `
                                            <div>
                                                <img src="img/ic-substituir.svg" alt="Sync">
                                            </div>
                                        ` : ``}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            `;
        });

        $('#product_list_container').html(html);
        NEApplication.showLoading(false);
    }

    //Product
    this.product = (product2) =>
    {
        NEApplication.showLoading(true);

        let html = `
        <div class="wrap commom">
            <div class="container">

                <div id="select_modal_container"></div>

                <div class="box-form-pedido">

                    <div class="row mt-3">
                        <div class="col-12">
                            <div class="unique-data">
                                <div class="lb-commom">
                                    Nome <span class="required">*</span>
                                </div>
                                <div class="content">
                                    <input id="product-name" type="text" class="form-control" value="${product2.name}">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mt-3">
                        <div class="col-12">
                            <div class="unique-data">
                                <div class="lb-commom">
                                    Código <span class="required">*</span>
                                </div>
                                <div class="content">
                                    <input id="product-id" type="text" class="form-control" value="${product2.id}">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mt-3">
                        <div class="col-12">
                            <div class="unique-data">
                                <div class="lb-commom">
                                    Preço de lista: <span class="required">*</span>
                                </div>
                                <div class="content">
                                    <input id="product-listPrice" type="text" class="form-control" value="${product2.listPrice}">
                                </div>
                            </div>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
        `
    }

    //Pesquisa Conta
    
    this.accountSearch = () => {
        NEApplication.showLoading(true);
        let html = `
            <div class="wrap commom" style="position: absolute; width: 100%; height: 100%;">
                <div class="container" style="height: 100%;">

                    <div class="row">
                        <div class="col-12">
                            <div class="unique-data">
                                <div class="content" style="padding: 0px 5px 15px 5px">
                                    <button class="btn btn-cta-primary shadow mb-2" onclick="NEAccountManager.createAccount()">
                                        <span>Criar Cliente</span>
                                        <img src="img/ic-plus-circle-white.svg" alt="Criar Cliente">
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="box-lista-search" id="account_list_container">


                    </div>
                </div>
            </div>
        `;

        $('#body_content').html(html);

        NEQueryController.searchAccounts();
    }
    

    //Desenha lista de contas
    
    this.writeAccountList = (accounts) => {
        let html = ``;

        if (accounts.length == 0)
            listHTML = '';

        accounts.forEach((account) => {
            html += `
                <div class="row">
                    <div class="mx-auto col-md-12 col-sm-12 col-xs-12 mb-2 mb-md-4">
                        <div class="card card-lista-pedido border-secondary">

                            <div class="card-body pt-3" onclick="NEAccountManager.editAccount('${account.ACCOUNT_ID}')">

                                <div class="row">
                                    <div class="col-12" style="display: flex;justify-content: space-between;">
                                        <div class="unique-data">
                                            <div class="content" style="color: #1f68b1">
                                                ${account.ACCOUNT_NAME}
                                            </div>
                                            <div class="lb-commom">
                                                Telefone: ${account.ACCOUNT_PHONE ? account.ACCOUNT_PHONE : 'Não informado'}
                                            </div>
                                            <div class="lb-commom">
                                                Tipo: ${account.ACCOUNT_TYPE ? account.ACCOUNT_TYPE : 'Não informado'}
                                            </div>
                                        </div>
                                        ${account.SYNC_STATUS == 0 ? `
                                            <div>
                                                <img src="img/ic-substituir.svg" alt="Sync">
                                            </div>
                                        ` : ``}
                                    </div>

                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            `;
        });

        $('#account_list_container').html(html);
        NEApplication.showLoading(false);
    }
    

    //Conta

    this.account = (account) => {
        NEApplication.showLoading(true);

        let html = `
            <div class="wrap commom">
                <div class="container">

                    <div id="select_modal_container"></div>

                    <div class="box-form-pedido">

                        <div class="row mt-3">
                            <div class="col-12">
                                <div class="unique-data">
                                    <div class="lb-commom">
                                        Nome <span class="required">*</span>
                                    </div>
                                    <div class="content">
                                        <input id="account-name" type="text" class="form-control" value="${account.name}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-3">
                            <div class="col-12">
                                <div class="unique-data">
                                    <div class="lb-commom">
                                        Telefone <span class="required">*</span>
                                    </div>
                                    <div class="content">
                                        <input id="account-phone" type="number" class="form-control" value="${account.phone}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-3">
                            <div class="col-12">
                                <div class="unique-data">
                                    <div class="lb-commom">
                                        Tipo do Cliente <span class="required">*</span>
                                    </div>
                                    <select id="account-type" class="form-control">

                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>


                    <div class="row mb-3 mt-4">
                        <div class="col-6 col-xs-6">
                            <button class="btn btn-outline-primary btn-nav-steps p-3 btn-block" onclick="NEBuildScreen.accountSearch()">
                                <img src="img/ic-arrow-left.svg" class="ic-voltar" alt="Voltar">
                                <span>Voltar</span>
                            </button>
                        </div>
                        <div class="col-6 col-xs-6 text-right">
                            <button class="btn btn-primary btn-nav-steps-blue p-3 btn-block" onclick="NEAccountManager.saveAccount()">
                                <span>Salvar</span>
                                <img src="img/ic-arrow-right.svg" class="ic-continuar" alt="Salvar">
                            </button>
                        </div>
                    </div>

                </div>
            </div>
        `;

        $('#body_content').html(html);

        NEQueryController.searchAccountTypes();

        NEApplication.showLoading(false);
        $(window).scrollTop(0);
    }
    

    //Pesquisa contato

    this.contactSearch = () => {
        NEApplication.showLoading(true);
        let html = `
            <div class="wrap commom" style="position: absolute;width: 100%;height: 100%;">
                <div class="container" style="height: 100%;">

                    <div class="row">
                        <div class="col-12">
                            <div class="unique-data">
                                <div class="content" style="padding: 0px 5px 15px 5px">
                                    <button class="btn btn-cta-primary shadow mb-2" onclick="NEContactManager.createContact()">
                                        <span>Criar Contato</span>
                                        <img src="img/ic-plus-circle-white.svg" alt="Criar Contato">
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="box-lista-search" id="contact_list_container">

                    </div>
                </div>
            </div>
        `;

        $('#body_content').html(html);

        NEQueryController.searchContacts();

    }
    

    //Desenha lista de contatos
    
    this.writeContactList = (contacts) => {
        let html = ``;

        if (contacts.length == 0)
            listHTML = '';

        contacts.forEach((contact) => {
            html += `
                <div class="row">
                    <div class="mx-auto col-md-12 col-sm-12 col-xs-12 mb-2 mb-md-4">
                        <div class="card card-lista-pedido border-secondary">

                            <div class="card-body pt-3" onclick="NEContactManager.editContact('${contact.CONTACT_ID}')">

                                <div class="row">
                                    <div class="col-12" style="display: flex;justify-content: space-between;">
                                        <div class="unique-data">
                                            <div class="content" style="color: #1f68b1">
                                                ${contact.CONTACT_FIRST_NAME + ' ' + contact.CONTACT_LAST_NAME}
                                            </div>
                                            <div class="lb-commom">
                                                Email: ${contact.CONTACT_EMAIL ? contact.CONTACT_EMAIL : 'Não informado'}
                                            </div>
                                            <div class="lb-commom">
                                                Cliente: ${contact.ACCOUNT_NAME ? contact.ACCOUNT_NAME : 'Não informado'}
                                            </div>
                                        </div>
                                        ${contact.SYNC_STATUS == 0 ? `
                                            <div>
                                                <img src="img/ic-substituir.svg" alt="Sync">
                                            </div>
                                        ` : ``}
                                    </div>

                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            `;
        });

        $('#contact_list_container').html(html);
        NEApplication.showLoading(false);
    }
    

    //Contato
    
    this.contact = (contact) => {
        NEApplication.showLoading(true);

        let html = `
            <div class="wrap commom">
                <div class="container">

                    <div id="select_modal_container"></div>

                    <div class="box-form-pedido">

                        <div class="row mt-3">
                            <div class="col-12">
                                <div class="unique-data">
                                    <div class="lb-commom">
                                        Nome <span class="required">*</span>
                                    </div>
                                    <div class="content">
                                        <input id="contact-first-name" type="text" class="form-control" value="${contact.firstName}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-3">
                            <div class="col-12">
                                <div class="unique-data">
                                    <div class="lb-commom">
                                        Sobrenome <span class="required">*</span>
                                    </div>
                                    <div class="content">
                                        <input id="contact-last-name" type="text" class="form-control" value="${contact.lastName}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-3">
                            <div class="col-12">
                                <div class="unique-data">
                                    <div class="lb-commom">
                                        Email <span class="required">*</span>
                                    </div>
                                    <div class="content">
                                        <input id="contact-email" type="email" class="form-control" value="${contact.email}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-3">
                            <div class="col-12">
                                <div class="unique-data">
                                    <div class="lb-commom">
                                        Cliente <span class="required">*</span>
                                    </div>
                                    <div class="content" onclick="NEBuildScreen.openSelectAccountModal()">
                                        <input id="contact-account" type="text" class="form-control" readonly value="${contact.accountName}">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>


                    <div class="row mb-3 mt-4">
                        <div class="col-6 col-xs-6">
                            <button class="btn btn-outline-primary btn-nav-steps p-3 btn-block" onclick="NEBuildScreen.contactSearch()">
                                <img src="img/ic-arrow-left.svg" class="ic-voltar" alt="Voltar">
                                <span>Voltar</span>
                            </button>
                        </div>
                        <div class="col-6 col-xs-6 text-right">
                            <button class="btn btn-primary btn-nav-steps-blue p-3 btn-block" onclick="NEContactManager.saveContact()">
                                <span>Salvar</span>
                                <img src="img/ic-arrow-right.svg" class="ic-continuar" alt="Salvar">
                            </button>
                        </div>
                    </div>

                </div>
            </div>
        `;

        $('#body_content').html(html);

        NEQueryController.searchAccountTypes();

        NEApplication.showLoading(false);
        $(window).scrollTop(0);
    }
    
    

    //Account - Type
    this.writeAccountTypeList = (accountTypes) => {
        let accountTypeValue = NEAccountManager.getType();
        let html = ``;

        if (!accountTypeValue) {
            html = `
                <option value="" disabled selected style="display: none;"></option>
            `;
        }

        accountTypes.forEach(item => {
            html += `
                <option value="${item.ACCOUNT_TYPE_IDX}" ${accountTypeValue == item.ACCOUNT_TYPE_IDX ? 'selected' : ''}>${item.ACCOUNT_TYPE_NAME}</option>
            `;
        });

        $('#account-type').html(html);
    }

    //Order - Status
    this.writeOrderStatusList = (orderStatus) => {
        let orderStatusValue = NEOrderManager.getStatus();
        let html = ``;

        if (!orderStatusValue) {
            html = `
                <option value="" disabled selected style="display: none;"></option>
            `;
        }

        orderStatus.forEach(item => {
            html += `
                <option value="${item.ORDER_STATUS_IDX}" ${orderStatusValue == item.ORDER_STATUS_IDX ? 'selected' : ''}>${item.ORDER_STATUS_NAME}</option>
            `;
        });

        $('#order-status').html(html);
    }

    //Order - Shipping
    this.writeOrderShippingList = (orderShipping) => {
        let orderShippingValue = NEOrderManager.getShipping();
        let html = ``;

        if (!orderShippingValue) {
            html = `
                <option value="" disabled selected style="display: none;"></option>
            `;
        }

        orderShipping.forEach(item => {
            html += `
                <option value="${item.ORDER_SHIPPING_IDX}" ${orderShippingValue == item.ORDER_SHIPPING_IDX ? 'selected' : ''}>${item.ORDER_SHIPPING_NAME}</option>
            `;
        });

        $('#order-shipping').html(html);
    }

    //Order - PaymentMethod
    this.writeOrderPaymentMethodList = (orderPaymentMethod) => {
        let orderPaymentMethodValue = NEOrderManager.getPaymentMethod();
        let html = ``;

        if (!orderPaymentMethodValue) {
            html = `
                <option value="" disabled selected style="display: none;"></option>
            `;
        }

        orderPaymentMethod.forEach(item => {
            html += `
                <option value="${item.ORDER_PAYMENTMETHOD_IDX}" ${orderPaymentMethodValue == item.ORDER_PAYMENTMETHOD_IDX ? 'selected' : ''}>${item.ORDER_PAYMENTMETHOD_NAME}</option>
            `;
        });

        $('#order-paymentMethod').html(html);
    }

    //Select Account Modal 
    this.openSelectAccountModal = () => {
        NEApplication.showLoading(true);
        let html = `
            <div class="modal modal-info" id="modal-select" tabindex="-1" role="dialog" style="display: none;">
                <div class="modal-dialog" role="document" style="height: 100%;display: flex;align-items: center;">
                    <div class="modal-content" style="padding: 20px;height: 80%;">

                        <div class="modal-header" style="padding: 0;">
                            <div class="row">
                                <div class="col-12">
                                    <div class="unique-data">
                                        <div class="content" style="padding: 0px 5px 15px 5px">
                                            <span>Selecione um cliente</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="button" class="close btn-modal" data-dismiss="modal" aria-label="Close" onclick="NEBuildScreen.closeSelectModal()">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div id="account_select_container" class="modal-body">


                        </div>

                    </div>
                </div>
            </div>
        `;
        $('#select_modal_container').html(html);
        $('#modal-select').show();

        NEQueryController.searchOrderAccounts();
    }

    //Escreve a lista de contas a serem selecionadas
    this.writeAccountListToSelect = (accounts) => {
        let html = ``;

        if (accounts.length == 0)
            html = ``;

        accounts.forEach((account) => {
            html += `
                <div class="row" style="padding-bottom: 5px;">
                    <div class="mx-auto col-md-12 col-sm-12 col-xs-12 mb-2 mb-md-4" style="padding: 0;">
                        <div class="card card-lista-pedido border-secondary">

                            <div class="card-body pt-3" onclick="NEOrderManager.addAccount('${account.ACCOUNT_IDX}', '${account.ACCOUNT_NAME}')">

                                <div class="row">
                                    <div class="col-12">
                                        <div class="unique-data">
                                            <div class="content" style="color: #1f68b1">
                                                ${account.ACCOUNT_NAME}
                                            </div>
                                            <div class="lb-commom">
                                                Telefone: ${account.ACCOUNT_PHONE ? account.ACCOUNT_PHONE : 'Não informado'}
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            `;
        });

        $('#account_select_container').html(html);
        NEApplication.showLoading(false);
    }

    //Abre modal de Produtos para OrderItem
    this.openSelectProduct2Modal = () =>
    {
        NEApplication.showLoading(true);
        let html = `
            <div class="modal modal-info" id="modal-select" tabindex="-1" role="dialog" style="display: none;">
                <div class="modal-dialog" role="document" style="height: 100%;display: flex;align-items: center;">
                    <div class="modal-content" style="padding: 20px;height: 80%;">

                        <div class="modal-header" style="padding: 0;">
                            <div class="row">
                                <div class="col-12">
                                    <div class="unique-data">
                                        <div class="content" style="padding: 0px 5px 15px 5px">
                                            <span>Selecione um produto</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="button" class="close btn-modal" data-dismiss="modal" aria-label="Close" onclick="NEBuildScreen.closeSelectModal()">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div id="product2_select_container" class="modal-body">


                        </div>

                    </div>
                </div>
            </div>
        `;
        $('#select_modal_container').html(html);
        $('#modal-select').show();

        NEQueryController.searchProductsOrderItems();
    }

    //Escreve a lista de produtos a serem selecionados
    this.writeProduct2ListToSelect = (products) =>
    {
        let html = ``;

        if (accounts.length == 0)
            html = ``;

        accounts.forEach((account) => {
            html += `
                <div class="row" style="padding-bottom: 5px;">
                    <div class="mx-auto col-md-12 col-sm-12 col-xs-12 mb-2 mb-md-4" style="padding: 0;">
                        <div class="card card-lista-pedido border-secondary">

                            <div class="card-body pt-3" onclick="NEOrderItemManager.addProduct2('${product2.PRODUCT2_IDX}', '${product2.PRODUCT2_NAME}')">

                                <div class="row">
                                    <div class="col-12">
                                        <div class="unique-data">
                                            <div class="content" style="color: #1f68b1">
                                                ${product2.PRODUCT2_NAME}
                                            </div>
                                            <div class="lb-commom">
                                                Preço de lista: ${product2.PRODUCT2_LISTPRICE ? product2.PRODUCT2_LISTPRICE : 'Não informado'}
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            `;
        });

        $('#product2_select_container').html(html);
        NEApplication.showLoading(false);
    }


    this.closeSelectModal = () => {
        $('#modal-select').hide();
    }

}

NEBuildScreen.VAR_instance = null;
NEBuildScreen.SUB_getInstance = function () {
    if (this.VAR_instance === null) this.VAR_instance = new NEBuildScreen();
    return this.VAR_instance;
};
NEBuildScreen.login = function () {
    NEBuildScreen.SUB_getInstance().login();
};
NEBuildScreen.welcome = function () {
    NEBuildScreen.SUB_getInstance().welcome();
};
//Conta
NEBuildScreen.accountSearch = function () {
    NEBuildScreen.SUB_getInstance().accountSearch();
};
NEBuildScreen.writeAccountList = function (accounts) {
    NEBuildScreen.SUB_getInstance().writeAccountList(accounts);
};
NEBuildScreen.writeAccountTypeList = function (accountTypes) {
    NEBuildScreen.SUB_getInstance().writeAccountTypeList(accountTypes);
};
//Contato
NEBuildScreen.contactSearch = function () {
    NEBuildScreen.SUB_getInstance().contactSearch();
};
NEBuildScreen.writeContactList = function (accounts) {
    NEBuildScreen.SUB_getInstance().writeContactList(accounts);
};
//this
NEBuildScreen.account = function (account) {
    NEBuildScreen.SUB_getInstance().account(account);
};
NEBuildScreen.contact = function (contact) {
    NEBuildScreen.SUB_getInstance().contact(contact);
};
NEBuildScreen.order = function (order) {
    NEBuildScreen.SUB_getInstance().order(order);
}
NEBuildScreen.orderItem = function (orderItem)
{
    NEBuildScreen.SUB_getInstance().orderItem(orderItem);
}
NEBuildScreen.product = function (pricebook) {
    NEBuildScreen.SUB_getInstance().product(pricebook);
}
//order
NEBuildScreen.orderSearch = function () {
    NEBuildScreen.SUB_getInstance().orderSearch();
};
NEBuildScreen.writeOrderList = function (orders) {
    NEBuildScreen.SUB_getInstance().writeOrderList(orders);
};
NEBuildScreen.writeOrderStatusList = function(orderStatus) {
    NEBuildScreen.SUB_getInstance().writeOrderStatusList(orderStatus);
};
NEBuildScreen.writeOrderShippingList = function(orderShippings) {
    NEBuildScreen.SUB_getInstance().writeOrderShippingList(orderShippings);
};
NEBuildScreen.writeOrderPaymentMethodList = function(orderPaymentMethods) {
    NEBuildScreen.SUB_getInstance().writeOrderPaymentMethodList(orderPaymentMethods);
};
//orderItem
NEBuildScreen.orderItemSearch = function (orderId)
{
    NEBuildScreen.SUB_getInstance().orderItemSearch(orderId);
}
NEBuildScreen.writeOrderItemList = function (orderItems, orderId)
{
    NEBuildScreen.SUB_getInstance().writeOrderItemList(orderitems,orderId);
}
//product2
NEBuildScreen.pricebookSearch = function() {
    NEBuildScreen.SUB_getInstance().pricebookSearch();
};
NEBuildScreen.writeProductList = function(products) {
    NEBuildScreen.SUB_getInstance().writeProductList(products);
}
//Modal
NEBuildScreen.openSelectAccountModal = function () {
    NEBuildScreen.SUB_getInstance().openSelectAccountModal();
};
NEBuildScreen.writeAccountListToSelect = function (account) {
    NEBuildScreen.SUB_getInstance().writeAccountListToSelect(account);
};
NEBuildScreen.closeSelectModal = function () {
    NEBuildScreen.SUB_getInstance().closeSelectModal();
};
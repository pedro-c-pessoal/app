function NEOrderItemManager() 
{

    this.orderItem = {};

    this.createOrderItem = (orderId) => 
    {
        this.orderItem = {
            quantity: '',
            listPrice: '',
            unitPrice: '',
            orderId: orderId,
            product2Id: ''
        };

        NEBuildScreen.orderItem(this.orderItem);
    }

    this.addOrder = (orderId, orderName) => {
        this.orderItem.orderId = orderId;
        this.orderItem.orderName = orderName;
        $('#orderItem-order').val(orderName);
        NEBuildScreen.closeSelectModal();
    }

    this.addProduct2 = (product2Id, product2Name) =>
    {
        this.orderItem.product2Id = product2Id;
        this.orderItem.product2Name = product2Name;
        $('#orderItem-product2').val(product2Name);
        NEBuildScreen.closeSelectModal();
    }

    this.saveOrderItem = () => 
    {
        NEApplication.showLoading(true);

        this.orderItem.quantity = $('#orderItem-quantity').val();
        this.orderItem.listPrice = $('#orderItem-listPrice').val();
        this.orderItem.unitPrice = $('#orderItem-unitPrice').val();

        if (NEOrderItemManager.verifyFields()) 
        {

            var orderItem = this.orderItem;

            let groupQuery = [];
            let orderItemBaseDMLInsert = 'INSERT INTO orderItem (ORDERITEM_QUANTITY, ORDERITEM_LISTPRICE, ORDERITEM_UNITPRICE, ORDER_IDX, PRODUCT2_IDX, SYNC_STATUS) VALUES ';
            let orderItemBaseDMLUpdate = 'UPDATE orderItem SET ';
            let orderItemQuery = ``;

            if (this.orderItem.id) 
            {
                orderItemQuery = `
                    ${orderItemBaseDMLUpdate}
                    ORDERITEM_QUANTITY = '${orderItem.quantity}',
                    ORDERITEM_LISTPRICE = '${orderItem.listPrice}',
                    ORDERITEM_UNITPRICE = '${orderItem.unitPrice}',
                    ORDER_IDX = '${orderItem.orderId}',
                    PRODUCT2_IDX = '${orderItem.product2Id}'
                    SYNC_STATUS = '0'
                    WHERE ORDER_ID = '${orderItem.id}'
                `;
            } 
            else 
            {
                orderItemQuery = `
                ${orderItemBaseDMLInsert}
                    (
                        '${SUB_stringEscape(orderItem.quantity)}',
                        '${SUB_stringEscape(orderItem.listPrice)}',
                        '${SUB_stringEscape(orderItem.unitPrice)}',
                        '${SUB_stringEscape(orderItem.orderId)}',
                        '${SUB_stringEscape(orderItem.product2Id)}',
                        '${SUB_stringEscape(0)}'
                    )
                `;
            }

            orderItemQuery = orderItemQuery.replace(/(\r\n|\n|\r)/gm, "");

            groupQuery.push(orderItemQuery);

            (new NEBulkSQL()).execute({
                statements: groupQuery,
                callback: function (resultSet, status) {
                    if (status.fails == 0) {
                        swal("Produto de pedido salvo com sucesso!", { icon: "success" }).then(() => {
                            NEApplication.showLoading(false);
                            NEBuildScreen.orderItemSearch();
                        });
                    } else {
                        swal("Erro na inserção do produto de pedido", { icon: "error" });
                        NEApplication.showLoading(false);
                    }
                }
            });

        } 
        else 
        {
            swal("Para salvar, preencha todos os campos obrigatórios: " + this.field, { icon: "warning" });
            NEApplication.showLoading(false);
        }

    }

    this.field = '';

    this.verifyFields = () => {
        this.field = '';
        let requiredFields = {
            'orderId': 'Pedido',
            'product2Id': 'Produto',
            'quantity': 'Quantidade',
            'listPrice': 'Preço de lista',
            'unitPrice': 'Preço da unidade'
        };
        let valid = true;
        Object.keys(requiredFields).forEach((field) => {
            if (!this.orderItem[field]) {
                this.field = requiredFields[field];
                valid = false;
            }
        });
        return valid;
    }


    this.editOrderItem = (orderItemId) => 
    {
        NEApplication.showLoading(true);
        NeDBManager.SUB_getOrderItemById(orderItemId, (response) => {
            let orderItem = response[0];

            this.orderItem = {
                id: orderItem.ORDERITEM_ID,
                quantity: orderItem.ORDERITEM_QUANTITY,
                listPrice: orderItem.ORDERITEM_LISTPRICE,
                unitPrice: orderItem.ORDERITEM_UNITPRICE,
                orderId: orderItem.ORDER_IDX,
                product2Id: orderItem.PRODUCT2_IDX
            };

            NEApplication.showLoading(false);
            NEBuildScreen.orderItem(this.orderItem);
        });
    }

}

NEOrderItemManager.VAR_instance = null;
NEOrderItemManager.SUB_getInstance = function () {
    if (this.VAR_instance === null) this.VAR_instance = new NEOrderItemManager();
    return this.VAR_instance;
};
NEOrderItemManager.createOrderItem = function () {
    NEOrderItemManager.SUB_getInstance().createOrderItem();
};
NEOrderItemManager.addOrder = function (orderId, orderName) {
    NEOrderItemManager.SUB_getInstance().addOrder(orderId, orderName);
};
NEOrderItemManager.addProduct2 = function (product2Id, product2Name) {
    NEOrderItemManager.SUB_getInstance().addProduct2(product2Id, product2Name);
};
NEOrderItemManager.verifyFields = function () {
    return NEOrderItemManager.SUB_getInstance().verifyFields();
};
NEOrderItemManager.saveOrderItem = function () {
    NEOrderItemManager.SUB_getInstance().saveOrderItem();
};
NEOrderItemManager.editOrderItem = function (orderItemId) {
    NEOrderItemManager.SUB_getInstance().editOrderItem(orderItemId);
}; 
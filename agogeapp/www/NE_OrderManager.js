function NEOrderManager() 
{

    this.order = {};

    this.createOrder = () => 
    {
        this.order = 
        {
            name: '',
            accountId: '',
            sellerInternalNumber: '',
            effectiveDate: '',
            deliverDate: '',
            status: '',
            shipping: '',
            ownerId: '',
            pricebook2Id: '',
            description: '',
            paymentMethod: ''
        };

        NEBuildScreen.order(this.order);
    }

    this.addPricebook = (pricebook2Id, pricebook2Name) =>
    {
        this.order.pricebook2Id = pricebook2Id;
        this.order.pricebook2Name = pricebook2Name;
        $('#order-priceBook').val(pricebook2Name);
        NEBuildScreen.closeSelectModal();
    }

    this.addAccount = (accountId, accountName) =>
    {
        this.order.accountId = accountId;
        this.order.accountName = accountName;
        $('#order-account').val(accountName);
        NEBuildScreen.closeSelectModal();
    }

    this.saveOrder = () => 
    {

        NEApplication.showLoading(true);

        this.order.name = $('#order-name').val();
        this.order.sellerInternalNumber = $('#order-sellerInternalNumber').val();
        this.order.effectiveDate = $('#order-effectiveDate').val();
        this.order.deliverDate = $('#order-deliverDate').val();
        this.order.status = $('#order-status').val();
        this.order.shipping = $('#order-shipping').val();
        this.order.ownerId = $('#order-ownerId').val();
        this.order.description = $('#order-description').val();
        this.order.paymentMethod =$('#order-paymentMethod').val();

        if (NEOrderManager.verifyFields()) 
        {

            var order = this.order;

            let groupQuery = [];
            let orderBaseDMLInsert = 'INSERT INTO ORDERTable (ORDER_NAME, ORDER_SELLERINTERNALNUMBER, ACCOUNT_IDX, ORDER_EFFECTIVEDATE, ORDER_DELIVERDATE, ' +
            'ORDER_STATUS, ORDER_SHIPPING, ORDER_OWNERID, ORDER_DESCRIPTION, ORDER_PAYMENTMETHOD, SYNC_STATUS) VALUES ';
            let orderBaseDMLUpdate = 'UPDATE OrderTable SET ';
            let orderQuery = '';

            if (order.id) 
            {
                orderQuery = `
                    ${orderBaseDMLUpdate}
                    ORDER_NAME = '${order.name}',
                    ORDER_SELLERINTERNALNUMBER = '${order.sellerInternalNumber}', 
                    ACCOUNT_IDX = '${order.accountId}',
                    ORDER_EFFECTIVEDATE = '${order.effectiveDate}', 
                    ORDER_DELIVERDATE = '${order.deliverDate}', 
                    ORDER_STATUS = '${order.status}', 
                    ORDER_SHIPPING = '${order.shipping}', 
                    ORDER_OWNERID = '${order.ownerId}', 
                    ORDER_DESCRIPTION = '${order.description}', 
                    ORDER_PAYMENTMETHOD = '${order.paymentMethod}', 
                    SYNC_STATUS = '0'
                    WHERE ORDER_ID = '${order.id}'
                `;
            } 
            else 
            {
                orderQuery = `
                ${orderBaseDMLInsert}
                    (
                        '${SUB_stringEscape(order.name)}',
                        '${SUB_stringEscape(order.sellerInternalNumber)}',
                        '${SUB_stringEscape(order.accountId)}',
                        '${SUB_stringEscape(order.effectiveDate)}',
                        '${SUB_stringEscape(order.deliverDate)}',
                        '${SUB_stringEscape(order.status)}',
                        '${SUB_stringEscape(order.shipping)}',
                        '${SUB_stringEscape(order.ownerId)}',
                        '${SUB_stringEscape(order.description)}',
                        '${SUB_stringEscape(order.paymentMethod)}',
                        '${SUB_stringEscape(0)}'
                    )
                `;
            }

            orderQuery = orderQuery.replace(/(\r\n|\n|\r)/gm, "");

            groupQuery.push(orderQuery);

            (new NEBulkSQL()).execute({
                statements: groupQuery,
                callback: function (resultSet, status) 
                {
                    var orderInternalId = order.id ? order.id : resultSet[0].insertId;

                    if (status.fails == 0) 
                    {

                        swal("Pedido salvo com sucesso! Em seguida, adicione produtos do pedido.", { icon: "success" }).then(() => 
                        {
                            NEApplication.showLoading(false);
                            NEBuildScreen.orderSearch();
                        });
                    } 
                    else 
                    {
                        swal("Erro na criação do pedido", { icon: "error" });
                        NEApplication.showLoading(false);
                    }
                }
            });

        } 
        else 
        {
            swal("Para salvar, preencha todos os campos obrigatórios: " + this.field, { icon: "warning" });
            NEApplication.showLoading(false);
        }

    }

    this.field = '';

    this.verifyFields = () => 
    {
        this.field = '';
        let requiredFields = 
        {
            'name': 'Nome',
            'sellerInternalNumber' : 'Número interno do vendedor',
            'effectiveDate' : 'Data de registro',
            'deliverDate' : 'Data de entrega',
            'status' : 'Status',
            'shipping' : 'Tipo de frete',
            'ownerId' : 'Proprietário',
            'description' : 'Descrição',
            'paymentMethod' : 'Método de pagamento'
        };
        let valid = true;
        Object.keys(requiredFields).forEach((field) => 
        {
            if (!this.order[field]) {
                this.field = requiredFields[field];
                valid = false;
            }
        });
        return valid;
    }

    this.getStatus = () => 
    {
        return this.order.status;
    }

    this.getPaymentMethod = () =>
    {
        return this.order.paymentMethod;
    }

    this.getShipping = () =>
    {
        return this.order.shipping;
    }

    this.editOrder = (orderId) => 
    {
        NEApplication.showLoading(true);
        NeDBManager.SUB_getOrderById(orderId, (response) => 
        {
            let order = response[0];

            this.order = 
            {
                id: order.ORDER_ID,
                name: order.ORDER_NAME,
                account: order.ACCOUNT_IDX,
                sellerInternalNumber: order.ORDER_SELLERINTERNALNUMBER,
                effectiveDate: order.ORDER_EFFECTIVEDATE,
                deliverDate: order.ORDER_DELIVERDATE,
                status: order.ORDER_STATUS,
                shipping: order.ORDER_SHIPPING,
                ownerId: order.ORDER_OWNERID,
                description: order.ORDER_DESCRIPTION,
                paymentMethod: order.ORDER_PAYMENTMETHOD
            };

            NEApplication.showLoading(false);
            NEBuildScreen.order(this.order);
        });
    }

}

NEOrderManager.VAR_instance = null;
NEOrderManager.SUB_getInstance = function () 
{
    if (this.VAR_instance === null) this.VAR_instance = new NEOrderManager();
    return this.VAR_instance;
};
NEOrderManager.createOrder = function () 
{
    NEOrderManager.SUB_getInstance().createOrder();
};
NEOrderManager.verifyFields = function () 
{
    return NEOrderManager.SUB_getInstance().verifyFields();
};
NEOrderManager.addAccount = function (accountId, accountName)
{
    return NEOrderManager.SUB_getInstance().addAccount(accountId, accountName);
};
NEOrderManager.addPricebook = function (priceBook2Id, priceBook2Name)
{
    return NEOrderManager.SUB_getInstance().addPricebook(priceBook2Id, priceBook2Name);
};
NEOrderManager.getStatus = function () 
{
    return NEOrderManager.SUB_getInstance().getStatus();
};
NEOrderManager.getShipping = function ()
{
    return NEOrderManager.SUB_getInstance().getShipping();
};
NEOrderManager.getPaymentMethod = function ()
{
    return NEOrderManager.SUB_getInstance().getPaymentMethod();
};
NEOrderManager.saveOrder = function () 
{
    NEOrderManager.SUB_getInstance().saveOrder();
};
NEOrderManager.editOrder = function (orderId) 
{
    NEOrderManager.SUB_getInstance().editOrder(orderId);
};
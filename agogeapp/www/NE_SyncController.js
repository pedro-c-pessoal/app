function NeSyncController() 
{
    this.syncRecords = 
    {
        lastSyncAccountId: '',
        lastSyncOrderId: '',
        lastSyncOrderItemId: '',
        lastSyncProduct2Id: ''
    }

    this.resetSyncRecords = function () 
    {
        this.syncRecords = 
        {
            lastSyncAccountId: '',
            lastSyncOrderId: '',
            lastSyncOrderItemId: '',
            lastSyncProduct2Id: ''
        }
    }

    this.syncData = function (lastSyncStamp, lastSyncId) 
    {
        return {
            lastSyncStamp: lastSyncStamp,
            lastSyncId: lastSyncId
        }
    }

    this.generateSyncQuery = function (received) 
    {
        var groupQuery = [];

        groupQuery = groupQuery.concat(this.syncBaseData(received));
        groupQuery = groupQuery.concat(this.syncSystemData(received));

        return groupQuery;
    }

    this.syncSystemData = function (received) 
    {
        var groupQuery = [];
        var query =
            'UPDATE system SET ' +
            '   USER_IDX = \'' + received.userData.userId + '\' '           +
            ' , USER_NAME = \'' + received.userData.userName + '\' '        +
            ' , USER_EMAIL = \'' + received.userData.userLogin + '\' '      +
            ' , USER_PROFILE = \'' + received.userData.userProfile + '\' '  +
            ' , LAST_SYNC_BASE_DATA = \'' + received.userData.lastSyncUser + '\' ';

        
        if (received.syncData.accountSync.lastSyncStamp) 
        {
            query += ' , LAST_SYNC_ACCOUNT = ' + received.syncData.accountSync.lastSyncStamp;
            query += ' , LAST_SYNC_ACCOUNT_ID = \'' + received.syncData.accountSync.lastSyncId + '\'';
        }
        
        if (received.syncData.orderSync.lastSyncStamp)
        {
            query += ' , LAST_SYNC_ORDER = ' + received.syncData.orderSync.lastSyncStamp;
            query += ' , LAST_SYNC_ORDER_ID = \'' + received.syncData.orderSync.lastSyncId + '\'';
        }

        if(received.syncData.orderItemSync.lastSyncStamp)
        {
            query += ' , LAST_SYNC_ORDERITEM = ' + received.syncData.orderItemSync.lastSyncStamp;
            query += ' , LAST_SYNC_ORDERITEM_ID = \'' + received.syncData.orderItemSync.lastSyncId + '\'';
        }

        if(received.syncData.product2Sync.lastSyncStamp)
        {
            query += ' , LAST_SYNC_PRODUCT2 = ' + received.syncData.product2Sync.lastSyncStamp;
            query += ' , LAST_SYNC_PRODUCT2_ID = \'' + received.syncData.product2Sync.lastSyncId + '\'';
        }
        
        query += ' WHERE SYSTEM_ID = 1';

        groupQuery.push
        (
            query
        );

        return groupQuery;
    }

    this.syncBaseData = function (received) 
    {

        var groupQuery = [];
        var baseQuery = 
        {
            'account': 'INSERT OR REPLACE INTO account (ACCOUNT_ID, ACCOUNT_IDX, ACCOUNT_NAME, ACCOUNT_PHONE, ACCOUNT_TYPE, SYNC_STATUS) VALUES',
            'account_type': 'INSERT OR REPLACE INTO account_type (ACCOUNT_TYPE_IDX, ACCOUNT_TYPE_NAME) VALUES',
            'order': 'INSERT OR REPLACE INTO ORDERTable (ORDER_ID, ORDER_IDX, ORDER_NAME, ORDER_SELLERINTERNALNUMBER, ORDER_EFFECTIVEDATE, ORDER_DELIVERDATE, ORDER_STATUS, ORDER_SHIPPING, ORDER_OWNERID, PRICEBOOK2_IDX, ORDER_DESCRIPTION, ORDER_PAYMENTMETHOD, SYNC_STATUS) VALUES',
            'order_status': 'INSERT OR REPLACE INTO order_status (ORDER_STATUS_IDX, ORDER_STATUS_NAME) VALUES',
            'order_shipping': 'INSERT OR REPLACE INTO order_shipping (ORDER_SHIPPING_IDX, ORDER_SHIPPING_NAME) VALUES',
            'order_paymentMethod': 'INSERT OR REPLACE INTO order_paymentMethod (ORDER_PAYMENTMETHOD_IDX, ORDER_PAYMENTMETHOD_NAME) VALUES',
            'orderItem': 'INSERT OR REPLACE INTO orderItem(ORDERITEM_ID, ORDERITEM_IDX, ORDERITEM_QUANTITY, ORDERITEM_LISTPRICE, ORDERITEM_UNITPRICE, ORDERITEM_DISCOUNT, ORDER_IDX, PRODUCT2_IDX, SYNC_STATUS) VALUES',
            'product2': 'INSERT OR REPLACE INTO product2(PRODUCT2_ID, PRODUCT2_IDX, PRODUCT2_NAME) VALUES'
        }
        queryBuffer = '',
        maxLength = 20000,
        currLength = 0,
        deletedArray = 
        {
            'account': [],
            'order': [],
            'orderItem': [],
            'product2': []
        };

        //ACCOUNT_ID, ACCOUNT_IDX, ACCOUNT_NAME, ACCOUNT_PHONE, ACCOUNT_TYPE
        if (received.accountRequest && received.accountRequest.id.length > 0) 
        {
            currLength = received.accountRequest.id.length;
            queryBuffer = baseQuery['account'];
            var hasBuffer = false;
            for (var i = 0; i < currLength; i++) 
            {
                if (received.accountRequest.deleted[i]) 
                {
                    deletedArray['account'].push(received.accountRequest.id[i]);
                } 
                else 
                {
                    queryBuffer += (hasBuffer ? ',' : '') + '('                                                                 +
                        ' (SELECT ACCOUNT_ID FROM account WHERE ACCOUNT_IDX = \'' + received.accountRequest.id[i] + '\' ), '    +
                        '\'' + SUB_stringEscape(received.accountRequest.id[i])                                                  +
                        '\',\'' + SUB_stringEscape(received.accountRequest.name[i])                                             +
                        '\',\'' + SUB_stringEscape(received.accountRequest.phone[i])                                            +
                        '\',\'' + SUB_stringEscape(received.accountRequest.type[i])                                             +
                        '\',\'' + SUB_stringEscape(1)                                                                           +
                        '\''                                                                                                    +
                        ')';
                    hasBuffer = true;
                    if (queryBuffer.length > maxLength) 
                    {
                        groupQuery.push(queryBuffer + ';');
                        queryBuffer = baseQuery['account'];
                        hasBuffer = false;
                    }
                }
            }
            if (hasBuffer) groupQuery.push(queryBuffer);
            if (deletedArray['account'].length > 0) 
            {
                groupQuery.push(
                    'DELETE FROM account ' +
                    ' WHERE ACCOUNT_IDX IN (\'' + deletedArray['account'].join('\',\'') + '\')'
                );
            }
        }

        //Account_type
        if (received.accountTypeRequest && received.accountTypeRequest.length > 0) 
        {
            groupQuery.push(' DELETE FROM account_type ');
            currLength = received.accountTypeRequest.length;
            queryBuffer = baseQuery['account_type'];
            var hasBuffer = false;
            for (var i = 0; i < currLength; i++) 
            {
                queryBuffer += (hasBuffer ? ',' : '') + '('                                     +
                    '\'' + SUB_stringEscape(received.accountTypeRequest[i].accountTypeId)       +
                    '\',\'' + SUB_stringEscape(received.accountTypeRequest[i].accountTypeName)  +
                    '\''                                                                        +
                    ')';
                hasBuffer = true;
                if (queryBuffer.length > maxLength) 
                {
                    groupQuery.push(queryBuffer + ';');
                    queryBuffer = baseQuery['account_type'];
                    hasBuffer = false;
                }
            }
            if (hasBuffer) groupQuery.push(queryBuffer);
        } 
        else groupQuery.push(' DELETE FROM account_type ');

        //Order
        if (received.orderRequest && received.orderRequest.id.length > 0)
        {
            currLength = received.orderRequest.id.length;
            queryBuffer = baseQuery['order'];
            var hasBuffer = false;
            for (var i = 0; i < currLength; i++) 
            {
                if (received.orderRequest.deleted[i]) 
                {
                    deletedArray['order'].push(received.orderRequest.id[i]);
                } 
                else 
                {
                    queryBuffer += (hasBuffer ? ',' : '') + '('                                                                 +
                        ' (SELECT ORDER_ID FROM OrderTable WHERE ORDER_IDX = \'' + received.orderRequest.id[i] + '\' ), '            +
                        '\'' + SUB_stringEscape(received.orderRequest.id[i])                                                    +
                        '\',\'' + SUB_stringEscape(received.orderRequest.name[i])                                               +
                        '\',\'' + SUB_stringEscape(received.orderRequest.sellerInternalNumber[i])                               +
                        '\',\'' + SUB_stringEscape(received.orderRequest.effectiveDate[i])                                      +
                        '\',\'' + SUB_stringEscape(received.orderRequest.deliverDate[i])                                        +
                        '\',\'' + SUB_stringEscape(received.orderRequest.status[i])                                             +
                        '\',\'' + SUB_stringEscape(received.orderRequest.shipping[i])                                           +
                        '\',\'' + SUB_stringEscape(received.orderRequest.ownerId[i])                                            +
                        '\',\'' + SUB_stringEscape(received.orderRequest.pricebook2Id[i])                                       +
                        '\',\'' + SUB_stringEscape(received.orderRequest.description[i])                                        +
                        '\',\'' + SUB_stringEscape(received.orderRequest.paymentMethod[i])                                      +   
                        '\',\'' + SUB_stringEscape(1)                                                                           +
                        '\''                                                                                                    +
                        ')';
                    hasBuffer = true;
                    if (queryBuffer.length > maxLength) 
                    {
                        groupQuery.push(queryBuffer + ';');
                        queryBuffer = baseQuery['order'];
                        hasBuffer = false;
                    }
                }
            }
            if (hasBuffer) groupQuery.push(queryBuffer);
            if (deletedArray['order'].length > 0) 
            {
                groupQuery.push(
                    'DELETE FROM ORDERTable ' +
                    ' WHERE ORDER_IDX IN (\'' + deletedArray['order'].join('\',\'') + '\')'
                );
            }
        }

        //Order_status
        if (received.orderStatusRequest && received.orderStatusRequest.length > 0) 
        {
            groupQuery.push(' DELETE FROM order_status ');
            currLength = received.orderStatusRequest.length;
            queryBuffer = baseQuery['order_status'];
            var hasBuffer = false;
            for (var i = 0; i < currLength; i++) 
            {
                queryBuffer += (hasBuffer ? ',' : '') + '('                                     +
                    '\'' + SUB_stringEscape(received.orderStatusRequest[i].orderStatusId)       +
                    '\',\'' + SUB_stringEscape(received.orderStatusRequest[i].orderStatusName)  +
                    '\''                                                                        +
                    ')';
                hasBuffer = true;
                if (queryBuffer.length > maxLength) 
                {
                    groupQuery.push(queryBuffer + ';');
                    queryBuffer = baseQuery['order_status'];
                    hasBuffer = false;
                }
            }
            if (hasBuffer) groupQuery.push(queryBuffer);
        } 
        else groupQuery.push(' DELETE FROM order_status ');

        //Order_shipping
        if (received.orderShippingRequest && received.orderShippingRequest.length > 0) 
        {
            groupQuery.push(' DELETE FROM order_shipping ');
            currLength = received.orderShippingRequest.length;
            queryBuffer = baseQuery['order_shipping'];
            var hasBuffer = false;
            for (var i = 0; i < currLength; i++) 
            {
                queryBuffer += (hasBuffer ? ',' : '') + '('                                     +
                    '\'' + SUB_stringEscape(received.orderShippingRequest[i].orderShippingId)       +
                    '\',\'' + SUB_stringEscape(received.orderShippingRequest[i].orderShippingName)  +
                    '\''                                                                        +
                    ')';
                hasBuffer = true;
                if (queryBuffer.length > maxLength) 
                {
                    groupQuery.push(queryBuffer + ';');
                    queryBuffer = baseQuery['order_shipping'];
                    hasBuffer = false;
                }
            }
            if (hasBuffer) groupQuery.push(queryBuffer);
        } 
        else groupQuery.push(' DELETE FROM order_shipping ');

        //Order_paymentMethod
        if (received.orderPaymentMethodRequest && received.orderPaymentMethodRequest.length > 0) 
        {
            groupQuery.push(' DELETE FROM order_paymentMethod ');
            currLength = received.orderPaymentMethodRequest.length;
            queryBuffer = baseQuery['order_paymentMethod'];
            var hasBuffer = false;
            for (var i = 0; i < currLength; i++) 
            {
                queryBuffer += (hasBuffer ? ',' : '') + '('                                     +
                    '\'' + SUB_stringEscape(received.orderPaymentMethodRequest[i].orderPaymentMethodId)       +
                    '\',\'' + SUB_stringEscape(received.orderPaymentMethodRequest[i].orderPaymentMethodName)  +
                    '\''                                                                        +
                    ')';
                hasBuffer = true;
                if (queryBuffer.length > maxLength) 
                {
                    groupQuery.push(queryBuffer + ';');
                    queryBuffer = baseQuery['order_paymentMethod'];
                    hasBuffer = false;
                }
            }
            if (hasBuffer) groupQuery.push(queryBuffer);
        } 
        else groupQuery.push(' DELETE FROM order_paymentMethod ');

        //OrderItem
        if (received.orderItemRequest && received.orderItemRequest.id.length > 0)
        {
            currLength = received.orderItemRequest.id.length;
            queryBuffer = baseQuery['orderItem'];
            var hasBuffer = false;
            for (var i = 0; i < currLength; i++) 
            {
                if (received.orderItemRequest.deleted[i]) 
                {
                    deletedArray['orderItem'].push(received.orderItemRequest.id[i]);
                } 
                else 
                {
                    queryBuffer += (hasBuffer ? ',' : '') + '('                                                                     +
                        ' (SELECT ORDERITEM_ID FROM orderItem WHERE ORDERITEM_IDX = \'' + received.orderItemRequest.id[i] + '\' ), '+
                        '\'' + SUB_stringEscape(received.orderItemRequest.id[i])                                                        +
                        '\',\'' + SUB_stringEscape(received.orderItemRequest.quantity[i])                                               +
                        '\',\'' + SUB_stringEscape(received.orderItemRequest.listPrice[i])                                              +
                        '\',\'' + SUB_stringEscape(received.orderItemRequest.unitPrice[i])                                              +
                        '\',\'' + SUB_stringEscape(received.orderItemRequest.discount[i])                                               +
                        '\',\'' + SUB_stringEscape(received.orderItemRequest.orderId[i])                                                +
                        '\',\'' + SUB_stringEscape(received.orderItemRequest.product2Id[i])                                             +
                        '\',\'' + SUB_stringEscape(1)                                                                               +
                        '\''                                                                                                        +
                        ')';
                    hasBuffer = true;
                    if (queryBuffer.length > maxLength) 
                    {
                        groupQuery.push(queryBuffer + ';');
                        queryBuffer = baseQuery['orderItem'];
                        hasBuffer = false;
                    }
                }
            }
            if (hasBuffer) groupQuery.push(queryBuffer);
            if (deletedArray['orderItem'].length > 0) 
            {
                groupQuery.push(
                    'DELETE FROM orderItem ' +
                    ' WHERE ORDERITEM_IDX IN (\'' + deletedArray['orderItem'].join('\',\'') + '\')'
                );
            }

            return groupQuery;
        }

        //Product2
        if (received.product2Request && received.product2Request.id.length > 0)
        {
            currLength = received.product2Request.id.length;
            queryBuffer = baseQuery['product2'];
            var hasBuffer = false;
            for (var i = 0; i < currLength; i++) 
            {
                if (received.product2Request.deleted[i]) 
                {
                    deletedArray['product2'].push(received.product2Request.id[i]);
                } 
                else 
                {
                    queryBuffer += (hasBuffer ? ',' : '') + '('                                                                     +
                        ' (SELECT PRODUCT2_ID FROM product2 WHERE PRODUCT2_IDX = \'' + received.product2Request.id[i] + '\' ), '        +
                        '\'' + SUB_stringEscape(received.product2Request.id[i])                                                      +
                        '\',\'' + SUB_stringEscape(received.product2Request.name[i])                                                 +
                        '\',\'' + SUB_stringEscape(1)                                                                               +
                        '\''                                                                                                        +
                        ')';
                    hasBuffer = true;
                    if (queryBuffer.length > maxLength) 
                    {
                        groupQuery.push(queryBuffer + ';');
                        queryBuffer = baseQuery['product2'];
                        hasBuffer = false;
                    }
                }
            }
            if (hasBuffer) groupQuery.push(queryBuffer);
            if (deletedArray['product2'].length > 0) 
            {
                groupQuery.push(
                    'DELETE FROM product2 ' +
                    ' WHERE PRODUCT2_IDX IN (\'' + deletedArray['product2'].join('\',\'') + '\')'
                );
            }

        }

        return groupQuery;
    }

    
}

NeSyncController.instance = null;

NeSyncController.getInstance = function () {
    if (NeSyncController.instance == null)
        NeSyncController.instance = new NeSyncController();

    return NeSyncController.instance;
}
NeSyncController.generateSyncQuery = function (received) {
    return NeSyncController.getInstance().generateSyncQuery(received);
}
NeSyncController.syncData = function (lastSyncStamp, lastSyncId) {
    return NeSyncController.getInstance().syncData(lastSyncStamp, lastSyncId);
}
NeSyncController.getSyncRecords = function () {
    return NeSyncController.getInstance().syncRecords;
}
NeSyncController.setSyncRecords = function (syncRecords) {
    NeSyncController.getInstance().syncRecords = syncRecords;
}
NeSyncController.resetSyncRecords = function () {
    return NeSyncController.getInstance().resetSyncRecords();
}

function SUB_stringEscape(VAR_string) {
    if (VAR_string == null || VAR_string == 'null') {
        return '';
    }
    return (typeof VAR_string == 'string' ?
        VAR_string.replace(/[\0\x08\x09\x1a\n\r"'\\\%]/g, function (char) {
            switch (char) {
                case "\0":
                    return "\\0";
                case "\x08":
                    return "\\b";
                case "\x09":
                    return "\\t";
                case "\x1a":
                    return "\\z";
                case "\n":
                    return "\\n";
                case "\r":
                    return "\\r";
                case "'":
                    return "''";
                case "\"":
                case "\\":
                case "%":
                    return "";
            }
        }) : VAR_string
    );
}
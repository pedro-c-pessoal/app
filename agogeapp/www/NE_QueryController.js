function NEQueryController() 
{
    this.accountList = [];
    this.orderList = [];
    this.orderItemList = [];
    this.product2List = [];

    //Accounts
    this.searchAccounts = function () {
        NeDBManager.SUB_getAccounts((accounts) => {
            this.accountList = accounts;
            NEBuildScreen.writeAccountList(this.accountList);
            APP.showLoading(false);
        });
    }

    this.getAccount = function (accountId) {
        return this.accountList.find(account => account.ACCOUNT_IDX == accountId);
    }

    //Account Types
    this.searchAccountTypes = function () {
        NeDBManager.SUB_getAccountTypes((accountTypes) => {
            NEBuildScreen.writeAccountTypeList(accountTypes);
        });
    }

    //Orders
    this.searchOrders = function () {
        NeDBManager.SUB_getOrders((orders) =>
        {
            this.orderList = orders;
            NEBuildScreen.writeOrderList(this.orderList);
            APP.showLoading(false);
        });
    }

    this.searchOrderItemsOrders = function ()
    {
        NeDBManager.SUB_getOrders((orders) => {
            this.orderItemOrderList = orders;
            if (this.orderItemOrderList.length > 0) {
                NEBuildScreen.writeOrderListToSelect(this.orderItemOrderList.filter(item => item.ORDER_IDX));
            } else {
                NEBuildScreen.writeOrderListToSelect(this.orderItemOrderList);
            }
            APP.showLoading(false);
        });
    }

    this.getOrder = function (orderId)
    {
        return this.orderList.find(order => order.ORDER_IDX == orderId);
    }

    //Order - Status
    this.searchOrderStatus = function ()
    {
        NeDBManager.SUB_getOrderStatus((orderStatus) => 
        {
            NEBuildScreen.writeOrderStatusList(orderStatus);
        });
    }

    //Order - Shipping
    this.searchOrderShipping = function ()
    {
        NeDBManager.SUB_getOrderShipping((orderShippings) =>
        {
            NEBuildScreen.writeOrderShippingList(orderShippings);
        });
    }

    //Order - Payment Method
    this.searchOrderPaymentMethod = function ()
    {
        NeDBManager.SUB_getOrderPaymentMethod((orderPaymentMethods) => 
        {
            NEBuildScreen.writeOrderPaymentMethodList(orderPaymentMethods);
        });
    }

    //Order - Account
    this.searchOrderAccounts = function ()
    {
        NeDBManager.SUB_getAccounts((accounts) => {
            this.accountOrderList = accounts;
            NEBuildScreen.writeAccountListToSelect(this.accountOrderList);
            APP.showLoading(false);
        });
    }

    //OrderItem
    this.searchOrderItems = function (orderId)
    {
        NeDBManager.SUB_getOrderItemFromOrderId((orderId, orderItems) => 
        {
            this.orderItemList = orderItems;
            NEBuildScreen.writeOrderItemList(this.orderItemList, orderId);
            APP.showLoading(false);
        });
    }

    this.searchProductsOrderItems = function()
    {
        NeDBManager.SUB_getOrderItems((orderItems) => {
            this.product2List = products;
            if (this.product2List.length > 0) 
            {
                NEBuildScreen.writeOrderItemListToSelect(this.productOrderItemList.filter(item => item.ORDERITEM_IDX));
            } 
            else 
            {
                NEBuildScreen.writeOrderItemListToSelect(this.productOrderItemList);
            }
            APP.showLoading(false);
        });
    }

    this.getOrderItem = function (orderItemId)
    {
        return this.orderItemList.find(orderItem => orderItem.ORDERITEM_IDX == orderItemId);
    }

    //Product
    this.searchProducts = function ()
    {
        NeDBManager.SUB_getProduct2((products) =>
        {
            this.productList = products;
            NEBuildScreen.writeProductList(this.productList);
            APP.showLoading(false);
        })
    }

    this.getProduct2 = function (product2Id)
    {
        return this.product2List.find(product2 => product2.PRODUCT2_IDX == product2Id);
    }

    //PricebookEntry
    this.searchPricebookEntry = function ()
    {
        NeDBManager.SUB_getPricebookEntry((pricebookEntries) =>
        {
            this.pricebookEntryList = pricebookEntries;
            app.showLoading(false);
        })
    }

    this.getPricebookEntry = function (pricebookEntryId)
    {
        return this.pricebookEntryList.find(pricebookEntry => pricebookEntry.PRICEBOOKENTRY_IDX == pricebookEntryId);
    }
}

NEQueryController.VAR_instance = null;
NEQueryController.SUB_getInstance = function () {
    if (this.VAR_instance === null) this.VAR_instance = new NEQueryController();
    return this.VAR_instance;
};

//Account
NEQueryController.searchAccounts = function () 
{
    NEQueryController.SUB_getInstance().searchAccounts();
};

NEQueryController.getAccount = function (accountId) 
{
    return NEQueryController.SUB_getInstance().getAccount(accountId);
};

NEQueryController.searchAccountTypes = function () 
{
    NEQueryController.SUB_getInstance().searchAccountTypes();
};

//Order
NEQueryController.searchOrders = function () 
{
    NEQueryController.SUB_getInstance().searchOrders();
}

NEQueryController.searchOrderItemsOrders = function ()
{
    NEQueryController.SUB_getInstance().searchOrderItemsOrders();
}

NEQueryController.getOrder = function (orderId)
{
    return NEQueryController.SUB_getInstance().getOrder(orderId);
}

NEQueryController.searchOrderStatus = function ()
{
    NEQueryController.SUB_getInstance().searchOrderStatus();
}

NEQueryController.searchOrderShipping = function ()
{
    NEQueryController.SUB_getInstance().searchOrderShipping();
}

NEQueryController.searchOrderPaymentMethod = function ()
{
    NEQueryController.SUB_getInstance().searchOrderPaymentMethod();
}

NEQueryController.searchOrderAccounts = function ()
{
    NEQueryController.SUB_getInstance().searchOrderAccounts();
}

//OrderItem

NEQueryController.searchOrderItems = function ()
{
    NEQueryController.SUB_getInstance().searchOrderItems();
}

NEQueryController.searchProductsOrderItems = function ()
{
    NEQueryController.SUB_getInstance().searchProductsOrderItems();
}

NEQueryController.getOrderItem = function (orderItemId)
{
    return NEQueryController.SUB_getInstance().getOrderItem(orderItemId);
}

//Product

NEQueryController.searchProducts = function ()
{
    NEQueryController.SUB_getInstance().searchProducts();
}

NEQueryController.getProduct2 = function (product2Id)
{
    return NEQueryController.SUB_getInstance().getProduct2(product2Id);
}

//PricebookEntry

NEQueryController.searchPricebookEntry = function ()
{
    NEQueryController.SUB_getInstance().searchPricebookEntry();
}

NEQueryController.getPricebookEntry = function (pricebookEntryId)
{
    NEQueryController.SUB_getInstance().getPricebookEntry();
}
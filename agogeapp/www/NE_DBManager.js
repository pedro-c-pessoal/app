function NeDBManager() {

	var OBJ_database = window.sqlitePlugin.openDatabase
	({
		name: "app.db",
		location: 1
	});

	this.SUB_getDatabase = function()
	{
		return OBJ_database;
	};

	this.extractArrayFromDBResult = function (dbArray) 
	{
		var arrayE = [];
		for (var m = 0; m < dbArray.rows.length; m++) 
		{
			arrayE.push(dbArray.rows.item(m));
		}
		return arrayE;
	}

	/*
	 ------------------------------
	|							   |
	|		  GET OBJECTS		   |
	|							   |
	 ------------------------------
	*/

	//Account
	this.SUB_getAccounts = function (VAR_callBack) 
	{
		var response = [];

		var accountQuery = 'SELECT ' 			 																	+
			'					ACCOUNT_ID, ' 	 																	+
			'					ACCOUNT_IDX, ' 																		+
			'					ACCOUNT_NAME, '  																	+
			'					ACCOUNT_PHONE, ' 																	+
			'					ACCOUNT_TYPE, ' 																	+
			'					account_type.ACCOUNT_TYPE_NAME, '													+
			'					account.SYNC_STATUS '																		+
			'				FROM account '		 																	+
			'				LEFT JOIN account_type on account_type.ACCOUNT_TYPE_IDX = account.ACCOUNT_TYPE ' 		+
			'				ORDER BY ACCOUNT_NAME ';

		OBJ_database.transaction(
			function (VAR_transaction) 
			{

				VAR_transaction.executeSql
				(
					accountQuery,
					[],
					function (VAR_trx, VAR_res) 
					{
						response = NeDBManager.extractArrayFromDBResult(VAR_res);
					}
				);
			},
			function (VAR_err) 
			{
				__errorHandler(VAR_err);
				VAR_callBack(VAR_err);
			},
			function () 
			{
				VAR_callBack(response);
			}
		)
	}

	//Account_type
	this.SUB_getAccountTypes = function (VAR_callBack) 
	{
		var response = [];

		var accountTypeQuery = 'SELECT ' 					+
			'						ACCOUNT_TYPE_IDX, ' 	+
			'						ACCOUNT_TYPE_NAME ' 	+
			'					FROM account_type ' 		+
			'					ORDER BY ACCOUNT_TYPE_NAME ';

		OBJ_database.transaction
		(
			function (VAR_transaction) 
			{

				VAR_transaction.executeSql
				(
					accountTypeQuery,
					[],
					function (VAR_trx, VAR_res) 
					{
						response = NeDBManager.extractArrayFromDBResult(VAR_res);
					}
				);
			},
			function (VAR_err) 
			{
				__errorHandler(VAR_err);
				VAR_callBack(VAR_err);
			},
			function () 
			{
				VAR_callBack(response);
			}
		)
	}

	//Order
	this.SUB_getOrders = function (VAR_callBack) 
	{
		var response = [];

		var orderQuery = 'SELECT ' 			 																		+
			'					ORDER_ID, ' 	 																	+
			'					ORDER_IDX, ' 																		+
			'					account.ACCOUNT_IDX, '																		+
			'					ORDER_NAME, '  																		+
			'					ORDER_SELLERINTERNALNUMBER, ' 														+
			'					ORDER_EFFECTIVEDATE, ' 																+
			'					ORDER_DELIVERDATE, '																+
			'					ORDER_STATUS, '																		+
			'					order_status.ORDER_STATUS_NAME, '													+
			'					ORDER_SHIPPING, '																	+
			'					order_shipping.ORDER_SHIPPING_NAME, '												+
			'					ORDER_OWNERID, '																	+
			'					PRICEBOOK2_IDX, '																	+
			'					ORDER_DESCRIPTION, '																+
			'					ORDER_PAYMENTMETHOD, '																+
			'					order_paymentMethod.ORDER_PAYMENTMETHOD_NAME, '										+
			'					ordertable.SYNC_STATUS '																		+
			'				FROM ORDERTable '		 																	+
			'					LEFT JOIN order_status on order_status.ORDER_STATUS_IDX = ORDERTable.ORDER_STATUS '	 	+
			'					LEFT JOIN order_shipping on order_shipping.ORDER_SHIPPING_IDX = ORDERTable.ORDER_SHIPPING '+
			'					LEFT JOIN order_paymentMethod on order_paymentMethod.ORDER_PAYMENTMETHOD_IDX = ORDERTable.ORDER_PAYMENTMETHOD ' +
			'					LEFT JOIN Account on account.ACCOUNT_IDX = ORDERTable.ACCOUNT_IDX '					+
			'				ORDER BY ORDER_NAME ';

		OBJ_database.transaction(
			function (VAR_transaction) 
			{

				VAR_transaction.executeSql
				(
					orderQuery,
					[],
					function (VAR_trx, VAR_res) 
					{
						response = NeDBManager.extractArrayFromDBResult(VAR_res);
					}
				);
			},
			function (VAR_err) 
			{
				__errorHandler(VAR_err);
				VAR_callBack(VAR_err);
			},
			function () 
			{
				VAR_callBack(response);
			}
		)
	}

	//Order_Status
	this.SUB_getOrderStatus = function (VAR_callBack) 
	{
		var response = [];

		var orderStatusQuery = 'SELECT ' 					+
			'						ORDER_STATUS_IDX, ' 	+
			'						ORDER_STATUS_NAME ' 	+
			'					FROM order_status ' 		+
			'					ORDER BY ORDER_STATUS_NAME ';

		OBJ_database.transaction
		(
			function (VAR_transaction) 
			{

				VAR_transaction.executeSql
				(
					orderStatusQuery,
					[],
					function (VAR_trx, VAR_res) 
					{
						response = NeDBManager.extractArrayFromDBResult(VAR_res);
					}
				);
			},
			function (VAR_err) 
			{
				__errorHandler(VAR_err);
				VAR_callBack(VAR_err);
			},
			function () 
			{
				VAR_callBack(response);
			}
		)
	}

	//Order_Shipping
	this.SUB_getOrderShipping = function (VAR_callBack) 
	{
		var response = [];

		var orderShippingQuery = 'SELECT ' 					+
			'						ORDER_SHIPPING_IDX, ' 	+
			'						ORDER_SHIPPING_NAME ' 	+
			'					FROM order_shipping ' 		+
			'					ORDER BY ORDER_SHIPPING_NAME ';

		OBJ_database.transaction
		(
			function (VAR_transaction) 
			{

				VAR_transaction.executeSql
				(
					orderShippingQuery,
					[],
					function (VAR_trx, VAR_res) 
					{
						response = NeDBManager.extractArrayFromDBResult(VAR_res);
					}
				);
			},
			function (VAR_err) 
			{
				__errorHandler(VAR_err);
				VAR_callBack(VAR_err);
			},
			function () 
			{
				VAR_callBack(response);
			}
		)
	}

	//Order_PaymentMethod
	this.SUB_getOrderPaymentMethod = function (VAR_callBack) 
	{
		var response = [];

		var orderPaymentMethodQuery = '	SELECT ' 					+
			'								ORDER_PAYMENTMETHOD_IDX, ' 	+
			'								ORDER_PAYMENTMETHOD_NAME ' 	+
			'							FROM order_paymentmethod ' 		+
			'							ORDER BY ORDER_PAYMENTMETHOD_NAME ';

		OBJ_database.transaction
		(
			function (VAR_transaction) 
			{

				VAR_transaction.executeSql
				(
					orderPaymentMethodQuery,
					[],
					function (VAR_trx, VAR_res) 
					{
						response = NeDBManager.extractArrayFromDBResult(VAR_res);
					}
				);
			},
			function (VAR_err) 
			{
				__errorHandler(VAR_err);
				VAR_callBack(VAR_err);
			},
			function () 
			{
				VAR_callBack(response);
			}
		)
	}

	//OrderItem
	this.SUB_getOrderItem = function (VAR_callBack) 
	{
		var response = [];

		var orderItemQuery = 'SELECT ' 					  +
			'						ORDERITEM_ID, ' 	  +
			'						ORDERITEM_IDX, ' 	  +
			'						ORDERITEM_QUANTITY, ' +
			'						ORDERITEM_LISTPRICE, '+
			'						ORDERITEM_UNITPRICE, '+
			'						ORDERITEM_DISCOUNT, ' +
			'						PRODUCT2_IDX, '		  +
			'						ORDER_IDX, '		  +
			'						SYNC_STATUS '		  +
			'					FROM orderitem ' 	 	  +
			'					ORDER BY ORDERITEM_IDX ';

		OBJ_database.transaction
		(
			function (VAR_transaction) 
			{

				VAR_transaction.executeSql
				(
					orderItemQuery,
					[],
					function (VAR_trx, VAR_res) 
					{
						response = NeDBManager.extractArrayFromDBResult(VAR_res);
					}
				);
			},
			function (VAR_err) 
			{
				__errorHandler(VAR_err);
				VAR_callBack(VAR_err);
			},
			function () 
			{
				VAR_callBack(response);
			}
		)
	}

	//Product2
	this.SUB_getProduct2 = function (VAR_callBack) 
	{
		var response = [];

		var product2Query = 'SELECT ' 					 	+
			'						PRODUCT2_ID, ' 	 		+
			'						PRODUCT2_IDX, ' 	  		+
			'						PRODUCT2_NAME ' 			+
			'					FROM Product2 ' 	 	  		+
			'					ORDER BY PRODUCT2_IDX ';

		OBJ_database.transaction
		(
			function (VAR_transaction) 
			{

				VAR_transaction.executeSql
				(
					product2Query,
					[],
					function (VAR_trx, VAR_res) 
					{
						response = NeDBManager.extractArrayFromDBResult(VAR_res);
					}
				);
			},
			function (VAR_err) 
			{
				__errorHandler(VAR_err);
				VAR_callBack(VAR_err);
			},
			function () 
			{
				VAR_callBack(response);
			}
		)
	}

	/*
	 ------------------------------
	|							   |
	|		   GET DATA			   |
	|							   |
	 ------------------------------
	*/

	//Account
	this.SUB_getAccountData = function (VAR_callBack) 
	{

		var accountQuery = 'SELECT ' 																				+
			'					ACCOUNT_ID, ' 																		+
			'					ACCOUNT_IDX, ' 																		+
			'					ACCOUNT_NAME, ' 																	+
			'					ACCOUNT_PHONE, ' 																	+
			'					ACCOUNT_TYPE, ' 																	+
			'					account_type.ACCOUNT_TYPE_NAME ' 													+
			'				FROM account ' 																			+
			'					LEFT JOIN account_type on account_type.ACCOUNT_TYPE_IDX = account.ACCOUNT_TYPE ' 	+
			'				WHERE account.SYNC_STATUS = 0 ';

		OBJ_database.transaction
		(
			function (VAR_transaction) 
			{

				VAR_transaction.executeSql
				(
					accountQuery,
					[],
					function (VAR_trx, VAR_res) 
					{
						response = NeDBManager.extractArrayFromDBResult(VAR_res);
					}
				);
			},
			function (VAR_err) 
			{
				__errorHandler(VAR_err);
				VAR_callBack(VAR_err);
			},
			function () 
			{
				VAR_callBack(response);
			}
		)
	}

	//Account by Id
	this.SUB_getAccountById = function (accountId, VAR_callBack) 
	{

		var accountQuery = 'SELECT ' 																				+
			'					ACCOUNT_ID, ' 																		+	
			'					ACCOUNT_IDX, ' 																		+
			'					ACCOUNT_NAME, ' 																	+
			'					ACCOUNT_PHONE, ' 																	+
			'					ACCOUNT_TYPE, ' 																	+
			'					account_type.ACCOUNT_TYPE_NAME ' 													+
			'				FROM account ' 																			+
			'					LEFT JOIN account_type on account_type.ACCOUNT_TYPE_IDX = account.ACCOUNT_TYPE ' 	+
			'				WHERE  account.ACCOUNT_ID = \'' + accountId + '\' ';

		OBJ_database.transaction
		(
			function (VAR_transaction) 
			{

				VAR_transaction.executeSql
				(
					accountQuery,
					[],
					function (VAR_trx, VAR_res) 
					{
						response = NeDBManager.extractArrayFromDBResult(VAR_res);
					}
				);
			},
			function (VAR_err) 
			{
				__errorHandler(VAR_err);
				VAR_callBack(VAR_err);
			},
			function () 
			{
				VAR_callBack(response);
			}
		)
	}

	//Order
	this.SUB_getOrderData = function (VAR_callBack) 
	{

		var orderQuery = 'SELECT ' 																					+
			'					ORDER_ID, ' 	 																	+
			'					ORDER_IDX, ' 																		+
			'					ORDER_NAME, '  																		+
			'					ORDERTABLE.ACCOUNT_IDX, '																		+
			'					account.ACCOUNT_NAME, '																+
			'					ORDER_SELLERINTERNALNUMBER, ' 														+
			'					ORDER_EFFECTIVEDATE, ' 																+
			'					ORDER_DELIVERDATE, '																+
			'					ORDER_STATUS, '																		+
			'					order_status.ORDER_STATUS_NAME, '													+
			'					ORDER_SHIPPING, '																	+
			'					order_shipping.ORDER_SHIPPING_NAME, '												+
			'					ORDER_OWNERID, '																	+
			'					PRICEBOOK2_IDX, '																	+
			'					ORDER_DESCRIPTION, '																+
			'					ORDER_PAYMENTMETHOD, '																+
			'					order_paymentMethod.ORDER_PAYMENTMETHOD_NAME '										+
			'				FROM ORDERTable ' 																			+
			'					LEFT JOIN order_status on order_status.ORDER_STATUS_IDX = ORDERTable.ORDER_STATUS '	 	+
			'					LEFT JOIN order_shipping on order_shipping.ORDER_SHIPPING_IDX = ORDERTable.ORDER_SHIPPING '+
			'					LEFT JOIN order_paymentMethod on order_paymentMethod.ORDER_PAYMENTMETHOD_IDX = ORDERTable.ORDER_PAYMENTMETHOD ' +
			'					LEFT JOIN account on account.ACCOUNT_IDX = ORDERTable.ACCOUNT_IDX '					+
			'				WHERE ORDERTable.SYNC_STATUS = 0 ';

		OBJ_database.transaction
		(
			function (VAR_transaction) 
			{

				VAR_transaction.executeSql
				(
					orderQuery,
					[],
					function (VAR_trx, VAR_res) 
					{
						response = NeDBManager.extractArrayFromDBResult(VAR_res);
					}
				);
			},
			function (VAR_err) 
			{
				__errorHandler(VAR_err);
				VAR_callBack(VAR_err);
			},
			function () 
			{
				VAR_callBack(response);
			}
		)
	}

	//Order by Id
	this.SUB_getOrderById = function (orderId, VAR_callback) 
	{

		var orderQuery = 'SELECT ' 																					+
			'					ORDER_ID, ' 	 																	+
			'					ORDER_IDX, ' 																		+
			'					ORDER_NAME, '  																		+
			'					account.ACCOUNT_IDX, '																+
			'					ORDER_SELLERINTERNALNUMBER, ' 														+
			'					ORDER_EFFECTIVEDATE, ' 																+
			'					ORDER_DELIVERDATE, '																+
			'					ORDER_STATUS, '																		+
			'					order_status.ORDER_STATUS_NAME, '													+
			'					ORDER_SHIPPING, '																	+
			'					order_shipping.ORDER_SHIPPING_NAME, '												+
			'					ORDER_OWNERID, '																	+
			'					PRICEBOOK2_IDX, '																	+
			'					ORDER_DESCRIPTION, '																+
			'					ORDER_PAYMENTMETHOD, '																+
			'					order_paymentMethod.ORDER_PAYMENTMETHOD_NAME '										+
			'				FROM ORDERTable ' 																			+
			'					LEFT JOIN order_status on order_status.ORDER_STATUS_IDX = ORDERTable.ORDER_STATUS '	 	+
			'					LEFT JOIN order_shipping on order_shipping.ORDER_SHIPPING_IDX = ORDERTable.ORDER_SHIPPING '+
			'					LEFT JOIN order_paymentMethod on order_paymentMethod.ORDER_PAYMENTMETHOD_IDX = ORDERTable.ORDER_PAYMENTMETHOD ' +
			'					LEFT JOIN account on account.ACCOUNT_IDX = ORDERTable.ACCOUNT_IDX '					+
			'				WHERE  ORDERTable.ORDER_ID = \'' + orderId + '\'  ';

		OBJ_database.transaction
		(
			function (VAR_transaction) 
			{

				VAR_transaction.executeSql (
					orderQuery,
					[],
					function (VAR_trx, VAR_res) {
						response = NeDBManager.extractArrayFromDBResult(VAR_res);
					}
				);
			},
			function (VAR_err) 
			{
				__errorHandler(VAR_err);
				VAR_callBack(VAR_err);
			},
			function () 
			{
				VAR_callBack(response);
			}
		)
	}

	//OrderItem
	this.SUB_getOrderItemData = function (VAR_callBack) 
	{
		var response = [];

		var orderItemQuery = 'SELECT ' 					  +
			'						ORDERITEM_ID, ' 	  +
			'						ORDERITEM_IDX, ' 	  +
			'						ORDERITEM_QUANTITY, ' +
			'						ORDERITEM_LISTPRICE, '+
			'						ORDERITEM_UNITPRICE, '+
			'						ORDERITEM_DISCOUNT, ' +
			'						ORDER_IDX, '		  +
			'						Product2.PRODUCT2_IDX '+
			'					FROM orderitem ' 	 	  +
			'					LEFT JOIN Product2 on Product2.PRODUCT2_IDX = orderItem.Product2_IDX'
			'					WHERE orderitem.SYNC_STATUS = 0 ';

		OBJ_database.transaction
		(
			function (VAR_transaction) 
			{

				VAR_transaction.executeSql
				(
					orderItemQuery,
					[],
					function (VAR_trx, VAR_res) 
					{
						response = NeDBManager.extractArrayFromDBResult(VAR_res);
					}
				);
			},
			function (VAR_err) 
			{
				__errorHandler(VAR_err);
				VAR_callBack(VAR_err);
			},
			function () 
			{
				VAR_callBack(response);
			}
		)
	}

	//OrderItem by Id
	this.SUB_getOrderItemById = function (orderItemId, VAR_callBack) 
	{
		var response = [];

		var orderItemQuery = 'SELECT ' 					  +
			'						ORDERITEM_ID, ' 	  +
			'						ORDERITEM_IDX, ' 	  +
			'						ORDERITEM_QUANTITY, ' +
			'						ORDERITEM_LISTPRICE, '+
			'						ORDERITEM_UNITPRICE, '+
			'						ORDERITEM_DISCOUNT, ' +
			'						ORDER_IDX, '		  +
			'						Product2.PRODUCT2_IDX '		  +
			'					FROM orderitem ' 	 	  +
			'					LEFT JOIN Product2 on Product2.Product2_IDX = orderItem.PRODUCT2_IDX ' +
			'					WHERE orderitem.ORDERITEM_ID = \'' + orderItemId + '\' ';

		OBJ_database.transaction
		(
			function (VAR_transaction) 
			{

				VAR_transaction.executeSql
				(
					orderItemQuery,
					[],
					function (VAR_trx, VAR_res) 
					{
						response = NeDBManager.extractArrayFromDBResult(VAR_res);
					}
				);
			},
			function (VAR_err) 
			{
				__errorHandler(VAR_err);
				VAR_callBack(VAR_err);
			},
			function () 
			{
				VAR_callBack(response);
			}
		)
	}

	this.SUB_getOrderItemFromOrderId = function(orderId, VAR_callBack)
	{
		var response = [];

		var orderItemQuery = 'SELECT ' 					  +
			'						ORDERITEM_ID, ' 	  +
			'						ORDERITEM_IDX, ' 	  +
			'						ORDERITEM_QUANTITY, ' +
			'						ORDERITEM_LISTPRICE, '+
			'						ORDERITEM_UNITPRICE, '+
			'						ORDERITEM_DISCOUNT, ' +
			'						orderitem.ORDER_IDX, '+
			'						ordertable.ORDER_ID, '+
			'						Product2.PRODUCT2_IDX '		  +
			'					FROM orderitem ' 	 	  +
			'					LEFT JOIN Product2 on Product2.Product2_IDX = orderItem.PRODUCT2_IDX ' +
			'					LEFT JOIN OrderTable on Ordertable.Order_IDX = orderitem.Order_IDX ' +
			'					WHERE ordertable.ORDER_ID = \'' + orderId + '\' ';

		OBJ_database.transaction
		(
			function (VAR_transaction) 
			{

				VAR_transaction.executeSql
				(
					orderItemQuery,
					[],
					function (VAR_trx, VAR_res) 
					{
						response = NeDBManager.extractArrayFromDBResult(VAR_res);
					}
				);
			},
			function (VAR_err) 
			{
				__errorHandler(VAR_err);
				VAR_callBack(VAR_err);
			},
			function () 
			{
				VAR_callBack(response);
			}
		)
	}

	//Product2
	this.SUB_getProduct2Data = function (VAR_callBack) 
	{
		var response = [];

		var product2Query = 'SELECT ' 					 	+
		'						PRODUCT2_ID, ' 	 		+
		'						PRODUCT2_IDX, ' 	  		+
		'						PRODUCT2_NAME ' 			+
		'					FROM Product2 ' 	 	  		+
		'					ORDER BY PRODUCT2_IDX ';

		OBJ_database.transaction
		(
			function (VAR_transaction) 
			{

				VAR_transaction.executeSql
				(
					product2Query,
					[],
					function (VAR_trx, VAR_res) 
					{
						response = NeDBManager.extractArrayFromDBResult(VAR_res);
					}
				);
			},
			function (VAR_err) 
			{
				__errorHandler(VAR_err);
				VAR_callBack(VAR_err);
			},
			function () 
			{
				VAR_callBack(response);
			}
		)
	}

	//Product2 by Id
	this.SUB_getProduct2ById = function (product2Id, VAR_callBack) 
	{
		var response = [];

		var product2Query = 'SELECT ' 					 	+
		'						PRODUCT2_ID, ' 	 		+
		'						PRODUCT2_IDX, ' 	  		+
		'						PRODUCT2_NAME ' 			+
		'					FROM Product2 ' 	 	  		+
		'					WHERE PRODUCT2_ID = \'' + product2Id + '\' ';


		OBJ_database.transaction
		(
			function (VAR_transaction) 
			{

				VAR_transaction.executeSql
				(
					product2Query,
					[],
					function (VAR_trx, VAR_res) 
					{
						response = NeDBManager.extractArrayFromDBResult(VAR_res);
					}
				);
			},
			function (VAR_err) 
			{
				__errorHandler(VAR_err);
				VAR_callBack(VAR_err);
			},
			function () 
			{
				VAR_callBack(response);
			}
		)
	}

	/*
	 ------------------------------
	|							   |
	|		    SYSTEM			   |
	|							   |
	 ------------------------------
	*/

	this.SUB_getSystemInfo = function(VAR_callBack)
	{
		var VAR_systemData = null;
		OBJ_database.transaction
		(
			function(VAR_transaction) 
			{
				VAR_transaction.executeSql
				(
					'SELECT '                   +
					'    API_HOST_URL, '        +
					'    USER_EMAIL, '          +
					'    USER_NAME, '           +
					'    USER_IDX, '            +
					'    USER_PROFILE, ' 		+
					'    REFRESH_TOKEN, '       +
					'    SESSION_KEY, '         +
                    '    LAST_SYNC_BASE_DATA '  +
					'  FROM system WHERE SYSTEM_ID = 1 ',
					[],
					function(VAR_trx, VAR_res)
					{
						if(VAR_res.rows.length > 0)
						{
							VAR_systemData = 
							{
								apiHost:					VAR_res.rows.item(0).API_HOST_URL,
								userEmail:					VAR_res.rows.item(0).USER_EMAIL,
								userName:					VAR_res.rows.item(0).USER_NAME,
								userIdx:					VAR_res.rows.item(0).USER_IDX,
								userProfile: 				VAR_res.rows.item(0).USER_PROFILE,
								refreshToken:				VAR_res.rows.item(0).REFRESH_TOKEN,
								sessionKey:					VAR_res.rows.item(0).SESSION_KEY,
                                lastSyncBase:               VAR_res.rows.item(0).LAST_SYNC_BASE_DATA,
							};
						}
					}
				);
			},
			function(VAR_err)
			{
				__errorHandler(VAR_err);
				VAR_callBack(VAR_systemData);
			},
			function()
			{
				VAR_callBack(VAR_systemData);
			}
		);
	};

	this.SUB_setConnectionInfo = function(VAR_data,VAR_callBack)
	{
		var VAR_done = false;
		OBJ_database.transaction
		(
			function(VAR_transaction) 
			{
				VAR_transaction.executeSql
				(
					'INSERT OR REPLACE INTO system (SYSTEM_ID, SYSTEM_VERSION, DB_VERSION, API_HOST_URL, REFRESH_TOKEN, SESSION_KEY) VALUES (?, ?, ?, ?, ?, ?)',
					[1, AppInfo.version, AppInfo.dbVersion, VAR_data.apiHost, VAR_data.refreshToken, VAR_data.sessionKey],
					function (VAR_trx, VAR_res) 
					{
						VAR_done = (VAR_res.rowsAffected == 1);
					}
				);
			},
			function(VAR_err)
			{
				__errorHandler(VAR_err);
				VAR_callBack(VAR_done);
			},
			function()
			{
				VAR_callBack(VAR_done);
			}
		);
	};

	this.SUB_getConnectionInfo = function(VAR_callBack)
	{
		var VAR_systemData = null;
		OBJ_database.transaction
		(
			function(VAR_transaction) 
			{
				VAR_transaction.executeSql
				(
					'	SELECT '                           	+
					'		API_HOST_URL, '                	+
					'		REFRESH_TOKEN, '               	+
					'		SESSION_KEY, '                 	+
					'		LAST_SYNC_BASE_DATA, '         	+
					'		LAST_SYNC_TRANSACTION_DATA, '  	+
					' 		LAST_SYNC_ACCOUNT, '           	+
					'		LAST_SYNC_ACCOUNT_ID, '        	+
					'		LAST_SYNC_ORDER, '				+
					'		LAST_SYNC_ORDER_ID, '			+
					'		LAST_SYNC_ORDERITEM, '			+
					'		LAST_SYNC_ORDERITEM_ID, '		+
					'		LAST_SYNC_PRODUCT2, '			+
					'		LAST_SYNC_PRODUCT2_ID '			+
					'	FROM system WHERE SYSTEM_ID = 1',
					[],
					function(VAR_trx, VAR_res)
					{
						if(VAR_res.rows.length > 0)
						{
							VAR_systemData = 
							{
								apiHost                     : VAR_res.rows.item(0).API_HOST_URL,
								refreshToken                : VAR_res.rows.item(0).REFRESH_TOKEN,
								sessionKey                  : VAR_res.rows.item(0).SESSION_KEY,
								lastSyncBaseData            : VAR_res.rows.item(0).LAST_SYNC_BASE_DATA,
								lastSyncTransactionData     : VAR_res.rows.item(0).LAST_SYNC_TRANSACTION_DATA,
								lastSyncAccountData         : VAR_res.rows.item(0).LAST_SYNC_ACCOUNT,
								lastSyncAccountDataId       : VAR_res.rows.item(0).LAST_SYNC_ACCOUNT_ID,
								lastSyncOrderData			: VAR_res.rows.item(0).LAST_SYNC_ORDER,
								lastSyncOrderDataId			: VAR_res.rows.item(0).LAST_SYNC_ORDER_ID,
								lastSyncOrderItemData		: VAR_res.rows.item(0).LAST_SYNC_ORDERITEM,
								lastSyncOrderItemDataId		: VAR_res.rows.item(0).LAST_SYNC_ORDERITEM_ID,
								lastSyncProduct2			: VAR_res.rows.item(0).LAST_SYNC_PRODUCT2,
								lastSyncProduct2Id			: VAR_res.rows.item(0).LAST_SYNC_PRODUCT2_ID
							};
						}
					}
				);
			},
			function(VAR_err)
			{
				__errorHandler(VAR_err);
				VAR_callBack(VAR_systemData);
			},
			function()
			{
				VAR_callBack(VAR_systemData);
			}
		);
	};

	this.SUB_initDatabase = function(VAR_callBack) 
	{
        console.log('create database');
		OBJ_database.transaction
		(
			function(VAR_transaction) 
			{
				VAR_transaction.executeSql
				(
					'CREATE TABLE IF NOT EXISTS system ( '						+
					'	SYSTEM_ID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, '	+
					'	SYSTEM_VERSION TEXT NOT NULL DEFAULT \'\', '			+
					'	DB_VERSION TEXT NOT NULL DEFAULT \'\', '				+
					'	API_HOST_URL TEXT NOT NULL DEFAULT \'\', '				+
					'	USER_EMAIL TEXT NOT NULL DEFAULT \'\', '				+
					'	USER_NAME TEXT NOT NULL DEFAULT \'\', '					+
					'	USER_IDX TEXT NOT NULL DEFAULT \'\', '					+
					'	USER_PROFILE TEXT NOT NULL DEFAULT \'\', ' 				+
					'	REFRESH_TOKEN TEXT NOT NULL DEFAULT \'\', '				+
					'	SESSION_KEY TEXT NOT NULL DEFAULT \'\', '				+
					'	LAST_SYNC_BASE_DATA NUMERIC DEFAULT 0, '				+
					'	LAST_SYNC_TRANSACTION_DATA NUMERIC DEFAULT 0, '			+

					' 	LAST_SYNC_ACCOUNT NUMERIC DEFAULT 0, ' 					+
					' 	LAST_SYNC_ACCOUNT_ID TEXT DEFAULT \'\', ' 				+

					'	LAST_SYNC_ORDER NUMERIC DEFAULT 0, '					+
					'	LAST_SYNC_ORDER_ID TEXT DEFAULT \'\', '					+

					'	LAST_SYNC_ORDERITEM NUMERIC DEFAULT 0, '				+
					'	LAST_SYNC_ORDERITEM_ID TEXT DEFAULT \'\', '				+

					'	LAST_SYNC_PRODUCT2 NUMERIC DEFAULT 0, '					+
					'	LAST_SYNC_PRODUCT2_ID TEXT DEFAULT \'\' ) ', [], function(VAR_res)
					{
						VAR_res.executeSql
						(
							'INSERT OR IGNORE INTO system (SYSTEM_ID, SYSTEM_VERSION, DB_VERSION, API_HOST_URL, USER_NAME, ' +
							'USER_EMAIL, USER_IDX, USER_PROFILE, REFRESH_TOKEN, SESSION_KEY) ' +
							'VALUES (1, "' + AppInfo.version + '", "' + AppInfo.dbVersion + '", "", "", "", "", "", "", "");'
						);
					}
				);

				VAR_transaction.executeSql
				(
					'CREATE TABLE IF NOT EXISTS sync_finger_print ( '	+
					'	TABLE_NAME TEXT NOT NULL DEFAULT \'\', '		+
					'	FINGER_PRINT TEXT NOT NULL DEFAULT \'\', '		+
					'	ID INTEGER NOT NULL DEFAULT 0, '				+
					'	PRIMARY KEY (TABLE_NAME,FINGER_PRINT) ) '
				);
				
				//Account Table
                VAR_transaction.executeSql
				(
                    'CREATE TABLE IF NOT EXISTS ACCOUNT ( ' 				+
					'    ACCOUNT_ID INTEGER PRIMARY KEY AUTOINCREMENT, ' 	+
                    '    ACCOUNT_IDX TEXT NOT NULL DEFAULT \'\', ' 			+
                    '    ACCOUNT_NAME TEXT NOT NULL DEFAULT \'\', ' 		+
                    '    ACCOUNT_PHONE TEXT NOT NULL DEFAULT \'\', ' 		+
                    '    ACCOUNT_TYPE TEXT NOT NULL DEFAULT \'\', ' 		+
                    '    DELETED NUMERIC NOT NULL DEFAULT 0, ' 				+
					' 	 SYNC_STATUS NUMERIC NOT NULL DEFAULT 0 ) '
                );

				//Account Type Table
                VAR_transaction.executeSql
				(
                    'CREATE TABLE IF NOT EXISTS ACCOUNT_TYPE ( ' 			+
                    '    ACCOUNT_TYPE_IDX TEXT NOT NULL DEFAULT \'\', ' 	+
                    '    ACCOUNT_TYPE_NAME TEXT NOT NULL DEFAULT \'\', ' 	+
                    '    PRIMARY KEY(ACCOUNT_TYPE_IDX) ) '
                );

				//Order Table
				VAR_transaction.executeSql
				(	
					'CREATE TABLE IF NOT EXISTS ORDERTable ( '									+
					'   ORDER_ID INTEGER PRIMARY KEY AUTOINCREMENT, '							+
					'	ORDER_IDX TEXT NOT NULL DEFAULT \'\', '									+
					'	ACCOUNT_IDX TEXT NOT NULL DEFAULT \'\', '								+
					'	ORDER_NAME TEXT NOT NULL DEFAULT \'\', '								+
					'	ORDER_SELLERINTERNALNUMBER TEXT NOT NULL DEFAULT \'\', ' 				+
					'	ORDER_EFFECTIVEDATE DATETIME NOT NULL DEFAULT SYSDATETIME, '			+
					'	ORDER_DELIVERDATE DATETIME NOT NULL DEFAULT SYSDATETIME, ' 				+
					'	ORDER_STATUS TEXT NOT NULL DEFAULT \'\', '								+
					'	ORDER_SHIPPING TEXT NOT NULL DEFAULT \'\', '							+
					'	ORDER_OWNERID TEXT NOT NULL DEFAULT \'\', '								+
					'	PRICEBOOK2_IDX TEXT NOT NULL DEFAULT \'\', '							+
					'	ORDER_DESCRIPTION TEXT NOT NULL DEFAULT \'\', '							+
					'	ORDER_PAYMENTMETHOD TEXT NOT NULL DEFAULT \'\', '						+
					'	DELETED NUMERIC NOT NULL DEFAULT 0, '									+
					'	SYNC_STATUS NUMERIC NOT NULL DEFAULT 0 ) '
				);

				//Order Status Table
				VAR_transaction.executeSql
				(
					'CREATE TABLE IF NOT EXISTS ORDER_STATUS ( ' 								+
					'	ORDER_STATUS_IDX TEXT NOT NULL DEFAULT \'\', '							+
					'	ORDER_STATUS_NAME TEXT NOT NULL DEFAULT \'\', '							+
					'	PRIMARY KEY(ORDER_STATUS_IDX) ) '
				);

				//Order Shipping Table
				VAR_transaction.executeSql
				(
					'CREATE TABLE IF NOT EXISTS ORDER_SHIPPING ( ' 																			+
					'	ORDER_SHIPPING_IDX TEXT NOT NULL DEFAULT \'\', '							+
					'	ORDER_SHIPPING_NAME TEXT NOT NULL DEFAULT \'\', '							+
					'	PRIMARY KEY(ORDER_SHIPPING_IDX) ) '
				);


				//Order PaymentMethod Table
				VAR_transaction.executeSql
				(
					'CREATE TABLE IF NOT EXISTS ORDER_PAYMENTMETHOD ( ' 																			+
					'	ORDER_PAYMENTMETHOD_IDX TEXT NOT NULL DEFAULT \'\', '						+
					'	ORDER_PAYMENTMETHOD_NAME TEXT NOT NULL DEFAULT \'\', '						+
					'	PRIMARY KEY(ORDER_PAYMENTMETHOD_IDX) ) '
				);

				//OrderItem Table
				VAR_transaction.executeSql
				(
					'CREATE TABLE IF NOT EXISTS ORDERITEM ( '										+
					'	ORDERITEM_ID INTEGER PRIMARY KEY AUTOINCREMENT, '							+
					'	ORDERITEM_IDX TEXT NOT NULL DEFAULT \'\', '									+
					'	ORDERITEM_QUANTITY DECIMAL(16,2) NOT NULL DEFAULT 0, '						+
					'	ORDERITEM_LISTPRICE DECIMAL(16,2) NOT NULL DEFAULT 0, '						+
					'	ORDERITEM_UNITPRICE DECIMAL(16,2) NOT NULL DEFAULT 0, '						+
					'	ORDERITEM_DISCOUNT DECIMAL(16,2) NOT NULL DEFAULT 0, '						+
					'	ORDER_IDX TEXT NOT NULL DEFAULT \'\', '										+
					'	PRODUCT2_IDX TEXT NOT NULL DEFAULT \'\', '									+
					'	DELETED NUMERIC NOT NULL DEFAULT 0, '										+
					'	SYNC_STATUS NUMERIC NOT NULL DEFAULT 0 ) '
				);

				//Product Table
				VAR_transaction.executeSql
				(
					'CREATE TABLE IF NOT EXISTS PRODUCT2 ( '											+
					'	PRODUCT2_ID INTEGER PRIMARY KEY AUTOINCREMENT, '								+
					'	PRODUCT2_IDX TEXT NOT NULL DEFAULT \'\', '									+
					'	PRODUCT2_NAME TEXT NOT NULL DEFAULT \'\', '									+
					'	SYNC_STATUS NUMERIC NOT NULL DEFAULT 0 ) '
				)

			},
			function (VAR_err) 
			{
				__errorHandler(VAR_err);
				VAR_callBack();
			},
			function () 
			{
				VAR_callBack();
			}
		);

		this.SUB_clearUserInfo = function (VAR_data, VAR_callBack) 
		{
			var VAR_done = false;
			
			OBJ_database.transaction
			(
				function (VAR_transaction) 
				{

					var VAR_sql =
						'UPDATE system SET ' +
						' USER_EMAIL     = "", ' +
						' USER_NAME      = "", ' +
						' API_HOST_URL   = "", ' +
						' REFRESH_TOKEN  = "", ' +
						' SYSTEM_VERSION = "", ' +
						' DB_VERSION     = "", ' +
						' SESSION_KEY    = ""  ' +
						(VAR_data ?
							' , USER_IDX                  	= ""' +
							' , LAST_SYNC_BASE_DATA       	= 0 ' +
							' , LAST_SYNC_TRANSACTION_DATA	= 0 ' +
							' , LAST_SYNC_ACCOUNT         	= 0 ' +
							' , LAST_SYNC_ACCOUNT_ID      	= 0 ' +
							' , LAST_SYNC_ORDER				= 0 ' +
							' , LAST_SYNC_ORDER_ID			= 0 ' +
							' , LAST_SYNC_ORDERITEM			= 0 ' +
							' , LAST_SYNC_ORDERITEM_ID		= 0 ' +
							' , LAST_SYNC_PRODUCT2			= 0 ' +
							' , LAST_SYNC_PRODUCT2_ID		= 0 '
							: 
							''
						) +
						'WHERE SYSTEM_ID = 1'
						;
					VAR_transaction.executeSql
					(
						VAR_sql,
						[],
						function (VAR_trx, VAR_res) 
						{
							VAR_done = (VAR_res.rowsAffected == 1);
						}
					);
				},
				function (VAR_err) 
				{
					__errorHandler(VAR_err);
					VAR_callBack(VAR_done);
				},
				function () 
				{
					VAR_callBack(VAR_done);
				}
			);
		};
	};

	this.SUB_executeSQL = function(VAR_query, VAR_params, VAR_done, VAR_error) 
	{
		OBJ_database.transaction(function(VAR_transaction) 
		{
			VAR_transaction.executeSql(VAR_query, VAR_params, VAR_done, VAR_error);
		});
	};

	this.SUB_close = function() 
	{
		OBJ_database.close();
	};

	this.SUB_dropDatabase = function (VAR_callback) 
	{
		var groupQuery = 
		[
			'DELETE FROM account',
			'DELETE FROM account_type',
			'DELETE FROM ORDERTable',
			'DELETE FROM order_status',
			'DELETE FROM order_shipping',
			'DELETE FROM order_paymentMethod',
			'DELETE FROM orderItem',
			'DELETE FROM product2'
		];

		(new NEBulkSQL()).execute
		(
			{
				statements: groupQuery,
				callback: function (resultSet, status) 
				{
					if (status.fails == 0) {
						VAR_callback(true);
					}
					else {
						VAR_callback(false)
					}
				}
			}
		);
	};
}

NeDBManager.VAR_instance = null;
NeDBManager.SUB_getInstance = function() 
{
	if (this.VAR_instance == null) this.VAR_instance = new NeDBManager();
	return this.VAR_instance;
};

//Database essentials
NeDBManager.SUB_initDatabase = function(VAR_callBack) 
{
	NeDBManager.SUB_getInstance().SUB_initDatabase(VAR_callBack);
};
NeDBManager.SUB_executeSQL = function(VAR_query, VAR_params, VAR_done, VAR_error) 
{
	NeDBManager.SUB_getInstance().SUB_executeSQL(VAR_query, VAR_params, VAR_done, VAR_error);
};
NeDBManager.SUB_close = function() 
{
	NeDBManager.SUB_getInstance().SUB_close();
};
NeDBManager.SUB_getDatabase = function() 
{
	return NeDBManager.SUB_getInstance().SUB_getDatabase();
};
NeDBManager.SUB_getConnectionInfo = function(VAR_callback) 
{
	NeDBManager.SUB_getInstance().SUB_getConnectionInfo(VAR_callback);
};
NeDBManager.SUB_setConnectionInfo = function(VAR_data,VAR_callback) 
{
	NeDBManager.SUB_getInstance().SUB_setConnectionInfo(VAR_data,VAR_callback);
};
NeDBManager.SUB_getSystemInfo = function(VAR_data,VAR_callback) 
{
	NeDBManager.SUB_getInstance().SUB_getSystemInfo(VAR_data,VAR_callback);
};
NeDBManager.SUB_clearUserInfo = function (VAR_data, VAR_callBack) 
{
	NeDBManager.SUB_getInstance().SUB_clearUserInfo(VAR_data, VAR_callBack);
}
NeDBManager.SUB_dropDatabase = function (VAR_callBack) 
{
	NeDBManager.SUB_getInstance().SUB_dropDatabase(VAR_callBack);
}
//Account
NeDBManager.SUB_getAccounts = function (VAR_callBack) 
{
	NeDBManager.SUB_getInstance().SUB_getAccounts(VAR_callBack);
}
//Account Types
NeDBManager.SUB_getAccountTypes = function (VAR_callBack) 
{
	NeDBManager.SUB_getInstance().SUB_getAccountTypes(VAR_callBack);
}
//Order
NeDBManager.SUB_getOrders = function (VAR_callBack)
{
	NeDBManager.SUB_getInstance().SUB_getOrders(VAR_callBack);
}
//Order - Status
NeDBManager.SUB_getOrderStatus = function (VAR_callBack)
{
	NeDBManager.SUB_getInstance().SUB_getOrderStatus(VAR_callBack);
}
//Order - Shipping
NeDBManager.SUB_getOrderShipping = function (VAR_callBack)
{
	NeDBManager.SUB_getInstance().SUB_getOrderShipping(VAR_callBack);
}
//Order - PaymentMethod
NeDBManager.SUB_getOrderPaymentMethod = function (VAR_callBack)
{
	NeDBManager.SUB_getInstance().SUB_getOrderPaymentMethod(VAR_callBack);
}
//OrderItem
NeDBManager.SUB_getOrderItem = function (VAR_callBack)
{
	NeDBManager.SUB_getInstance().SUB_getOrderItem(VAR_callBack);
}
//Product2
NeDBManager.SUB_getProduct2 = function (VAR_callBack)
{
	NeDBManager.SUB_getInstance().SUB_getProduct2(VAR_callBack);
}

//DATA

//Account data
NeDBManager.SUB_getAccountData = function (VAR_callBack) 
{
	NeDBManager.SUB_getInstance().SUB_getAccountData(VAR_callBack);
}
NeDBManager.SUB_getAccountById = function (accountId, VAR_callBack) 
{
	NeDBManager.SUB_getInstance().SUB_getAccountById(accountId, VAR_callBack);
}
//Order data
NeDBManager.SUB_getOrderData = function (VAR_callBack)
{
	NeDBManager.SUB_getInstance().SUB_getOrderData(VAR_callBack);
}
NeDBManager.SUB_getOrderById = function (orderId, VAR_callBack)
{
	NeDBManager.SUB_getInstance().SUB_getOrderById(orderId, VAR_callBack);
}
//OrderItem data
NeDBManager.SUB_getOrderItemData = function (VAR_callBack)
{
	NeDBManager.SUB_getInstance().SUB_getOrderItemData(VAR_callBack);
}
NeDBManager.SUB_getOrderItemById = function (orderItemId, VAR_callBack)
{
	NeDBManager.SUB_getInstance().SUB_getOrderItemById(orderItemId, VAR_callBack);
}
NeDBManager.SUB_getOrderItemFromOrderId = function (orderId, VAR_callBack)
{
	NeDBManager.SUB_getInstance().SUB_getOrderItemFromOrderId(orderId, VAR_callBack);
}
//Product2 Data
NeDBManager.SUB_getProduct2Data = function (VAR_callBack)
{
	NeDBManager.SUB_getInstance().SUB_getProduct2Data(VAR_callBack);
}
NeDBManager.SUB_getProduct2ById = function (product2Id, VAR_callBack)
{
	NeDBManager.SUB_getInstance().SUB_getProduct2ById(VAR_callBack);
}

//Array from DB
NeDBManager.extractArrayFromDBResult = function (dbArray) 
{
	return NeDBManager.SUB_getInstance().extractArrayFromDBResult(dbArray);
}

var __errorHandler = function(e)
{
	var error = ( typeof e.code 	!= 'undefined' ? '( code: ' + e.code + ' ) ' : '' );
	error 	 += ( typeof e.message  != 'undefined' ? e.message : e );
	showInfoPopup('Internal Error', error);
    APP.showLoading(false);
};